package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.confluence.event.events.content.ContentEvent;
import com.atlassian.confluence.event.events.types.ConfluenceEntityUpdated;
import com.atlassian.confluence.event.events.types.Removed;
import com.atlassian.confluence.event.events.types.Trashed;
import com.atlassian.confluence.event.events.types.Updated;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.google.common.collect.ImmutableMap;

public class CachingDetailsManager
{
    private static final String CACHE_NAME = CachingDetailsManager.class.getName();

    private final Cache cache;

    public CachingDetailsManager(CacheManager cacheManager, EventPublisher eventPublisher)
    {
        cache = cacheManager.getCache(CACHE_NAME);
        eventPublisher.register(this);
    }

    public void put(final long pageId, final ImmutableMap<String, ImmutableMap<String, ExtractedDetail>> value)
    {
        cache.put(pageId, value);
    }

    /**
     * Gets all the properties for a given page id
     */
    public ImmutableMap<String, ImmutableMap<String, ExtractedDetail>> get(final long pageId)
    {
        return (ImmutableMap<String, ImmutableMap<String, ExtractedDetail>>) cache.get(pageId);
    }

    @EventListener
    public void onPageUpdate(ContentEvent event)
    {
        if (event instanceof Updated || event instanceof Removed || event instanceof Trashed || event instanceof ConfluenceEntityUpdated)
            invalidateCache(event.getContent().getId());
    }

    public void removeAll()
    {
        cache.removeAll();
    }

    private void invalidateCache(long id)
    {
        cache.remove(id);
    }
}
