package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.PartialList;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.Labelable;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.user.User;
import com.google.common.collect.Sets;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import static com.atlassian.confluence.labels.LabelManager.NO_MAX_RESULTS;
import static com.atlassian.confluence.labels.LabelManager.NO_OFFSET;
import static com.atlassian.confluence.util.GeneralUtil.htmlEncode;
import static com.atlassian.confluence.util.LabelUtil.isValidLabelName;
import static com.google.common.base.Predicates.instanceOf;
import static com.google.common.collect.Collections2.filter;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Arrays.asList;
import static java.util.Collections.sort;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.isEmpty;
import static org.apache.commons.lang.StringUtils.split;
import static org.apache.commons.lang.StringUtils.trim;

public class ContentRetriever
{
    private final LabelManager labelManager;
    private final SpaceManager spaceManager;
    private final PermissionManager permissionManager;
    private final LocaleManager localeManager;
    private final I18NBeanFactory i18NBeanFactory;

    public ContentRetriever(LabelManager labelManager, SpaceManager spaceManager, PermissionManager permissionManager,
        LocaleManager localeManager, I18NBeanFactory i18NBeanFactory)
    {
        this.labelManager = labelManager;
        this.spaceManager = spaceManager;
        this.permissionManager = permissionManager;
        this.localeManager = localeManager;
        this.i18NBeanFactory = i18NBeanFactory;
    }

    public List<String> getSpaceKeysFromDelimitedString(final String spacesString)
    {
        final String spacesParameter = trim(spacesString);
        if (isBlank(spacesParameter)) {
            return null;
        }
        // Split by a range of common delimiters
        return asList(split(spacesParameter, ",;/| "));
    }

    public List<ContentEntityObject> getContentWithMetaData(@Nonnull final String labelString, final List<String> spaceKeys, final String spaceKey) throws MacroExecutionException
    {
        final List<? extends Labelable> labelledContent = getContent(labelString, spaceKeys, spaceKey);

        return filterAndSort(labelledContent, ContentEntityObject.class);
    }

    private List<? extends Labelable> getContent(@Nonnull final String labelString, final List<String> spaceKeys, final String spaceKey) throws MacroExecutionException
    {
        final Label label = validateLabel(trim(labelString));

        Set<String> keys;
        if (spaceKeys == null || spaceKeys.isEmpty())
        {
            final Space space = spaceManager.getSpace(spaceKey);
            if (space == null)
            {
                throw new MacroExecutionException("ConversionContext returned invalid space key '" + spaceKey + "'");
            }
            keys = Sets.newHashSet(spaceKey);
        }
        else if (spaceKeys.contains("@all"))
        {
            keys = Collections.emptySet();
        }
        else {
            keys = Sets.newHashSet(spaceKeys);
        }

        PartialList<ContentEntityObject> partialList = labelManager.getContentInSpacesForAllLabels(NO_OFFSET, NO_MAX_RESULTS, keys, label);
        return partialList == null? Collections.EMPTY_LIST : partialList.getList();
    }

    private <T extends Comparable> List<T> filterAndSort(List<? extends Labelable> labelledContent, Class<T> requiredType)
    {
        // Most likely everything returned by the labelManager will be a CEO, but due to the interface we have to
        // jump through a few hoops to get the final collection.
        final List filteredByType = newArrayList(filter(labelledContent, instanceOf(requiredType)));

        List permittedEntities = permissionManager.getPermittedEntities(getAuthenticatedUser(), Permission.VIEW, filteredByType);
        sort(permittedEntities, new LabelledContentComparator());

        return permittedEntities;
    }

    private Label validateLabel(final String labelName) throws MacroExecutionException
    {
        I18NBean i18NBean = i18NBeanFactory.getI18NBean(localeManager.getLocale(getAuthenticatedUser()));

        if (isEmpty(labelName))
            throw new MacroExecutionException(i18NBean.getText("detailsmacro.error.no.label.specified"));

        if (!isValidLabelName(labelName))
            throw new MacroExecutionException(i18NBean
                    .getText("detailsmacro.error.label.name.not.valid", new String[]{htmlEncode(labelName)}));

        final Label label = labelManager.getLabel(new Label(labelName));
        if (null == label)
            throw new MacroExecutionException(i18NBean
                    .getText("detailssummary.error.labeldoesnotexist", new String[]{htmlEncode(labelName)}));

        return label;
    }

    private static class LabelledContentComparator implements Comparator<ContentEntityObject>
    {
        @Override
        public int compare(ContentEntityObject ceo1, ContentEntityObject ceo2)
        {
            return ceo2.getLastModificationDate().compareTo(ceo1.getLastModificationDate());
        }
    }

    private User getAuthenticatedUser()
    {
        return AuthenticatedUserThreadLocal.get();
    }
}
