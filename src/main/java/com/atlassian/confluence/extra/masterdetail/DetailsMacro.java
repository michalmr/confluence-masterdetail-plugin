package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import org.apache.commons.lang.BooleanUtils;

import java.util.Map;

public class DetailsMacro extends BaseMacro implements Macro
{
    public static final String MASTERDETAIL_RESOURCES = "confluence.extra.masterdetail:master-details-resources";

    private final WebResourceManager webResourceManager;

    public DetailsMacro(WebResourceManager webResourceManager)
    {
        this.webResourceManager = webResourceManager;
    }

    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
    {
        try
        {
            return execute(parameters, body, new DefaultConversionContext(renderContext));
        }
        catch (MacroExecutionException macroError)
        {
            throw new MacroException(macroError);
        }
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.ALL;
    }

    public boolean hasBody()
    {
        return true;
    }

    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context)
    {
        return TokenType.BLOCK;
    }

    public String execute(Map<String, String> parameters, String bodyText, ConversionContext conversionContext) throws MacroExecutionException
    {
        String id = parameters.get(DetailsSummaryMacro.PARAM_ID);
        if (id != null && id.length() > DetailsSummaryMacro.PARAM_ID_MAX_LENGTH)
            return RenderUtils.blockError(GeneralUtil.getI18n().getText("details.error.id.length"), "");

        if (!BooleanUtils.toBoolean(parameters.get("hidden")))
        {
            webResourceManager.requireResource(MASTERDETAIL_RESOURCES);
            return new StringBuilder("<div class='plugin-tabmeta-details'>").append(bodyText).append("</div>").toString();
        }
        return "";
    }

    public BodyType getBodyType()
    {
        return BodyType.RICH_TEXT;
    }

    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }
}
