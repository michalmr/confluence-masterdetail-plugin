package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.content.render.xhtml.Namespace;
import com.atlassian.confluence.content.render.xhtml.XhtmlConstants;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringEscapeUtils;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.XPath;
import org.dom4j.io.SAXReader;
import org.dom4j.tree.DefaultElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
* Extracts all page properties for the given content. Tables in {details} macros are converted into a map of id to properties.
 * Empty string is the default id for properties that do not have one specified.
*/
class DetailsMacroBodyHandler implements MacroDefinitionHandler
{
    private static final Logger LOG = LoggerFactory.getLogger(DetailsMacroBodyHandler.class);

    public static final String CHARSET_UTF8 = "UTF-8";

    private Map<String, ImmutableMap<String, ExtractedDetail>> detailsById;

    private static final String DEFAULT_NAMESPACE_PREFIX = "xml";
    private static final String XPATH_TBODY = "//" + DEFAULT_NAMESPACE_PREFIX + ":tbody";

    private static final Map<String, String> NAMESPACE_MAP;

    static
    {
        NAMESPACE_MAP = new ConcurrentHashMap<String, String>(XhtmlConstants.STORAGE_NAMESPACES.size());
        for (Namespace namespace : XhtmlConstants.STORAGE_NAMESPACES)
        {
            NAMESPACE_MAP.put(namespace.getPrefix() != null ? namespace.getPrefix() : DEFAULT_NAMESPACE_PREFIX, namespace.getUri());
        }
    }

    DetailsMacroBodyHandler()
    {
        this.detailsById = new HashMap<String, ImmutableMap<String, ExtractedDetail>>();
    }

    public void handle(MacroDefinition macroDefinition)
    {
        if (!"details".equals(macroDefinition.getName()))
            return;

        String bodyText = macroDefinition.getBodyText();
        String detailsId = StringUtils.trim(macroDefinition.getParameter(DetailsSummaryMacro.PARAM_ID));

        if (detailsId == null)
            detailsId =  "";

        if (StringUtils.isBlank(bodyText))
        {
            addToDetails(detailsId, ImmutableMap.<String, ExtractedDetail>of());
            return;
        }
        try
        {
            ImmutableMap<String, ExtractedDetail> extractedDetails = extractDetails(bodyText);
            addToDetails(detailsId, extractedDetails);
        }
        catch (Exception e)
        {
            LOG.error(String.format("Unable to parse detailsById in detailsById macro\n%s", bodyText), e);
        }
    }

    /**
     * Get the detailsById extracted from the content. It is a mapping of ids to a map of heading/values.
     * The default without an id mapping is keyed with the empty string. Note null is returned if no details
     * for the given id are found.
     */
    public ImmutableMap<String, ExtractedDetail> getDetails(String detailsId)
    {
        ImmutableMap<String, ExtractedDetail> map = detailsById.get(detailsId);
        if (map == null)
            return null;

        return ImmutableMap.copyOf(map);
    }

    /**
     * @return an ImmutableMap keyed by detail ID to a map of details, where details is a map keyed by heading text to
     * ExtractedDetail objects
     */
    public ImmutableMap<String, ImmutableMap<String, ExtractedDetail>> getDetails()
    {
        return ImmutableMap.copyOf(detailsById);
    }

    private void addToDetails(String id, ImmutableMap<String, ExtractedDetail> details)
    {
        Map<String, ExtractedDetail> currentDetails = detailsById.get(id);
        if (currentDetails == null)
            detailsById.put(id, details);
        else
            detailsById.put(id, ImmutableMap.<String, ExtractedDetail>builder().putAll(currentDetails).putAll(details).build());
    }

    private ImmutableMap<String, ExtractedDetail> extractDetails(String macroBodyXhtml)
        throws IOException, DocumentException, ParserConfigurationException, SAXException
    {
        final Document macroBodyDoc = getMacroBodyDocument(macroBodyXhtml);

        final XPath xpath = macroBodyDoc.createXPath(XPATH_TBODY);
        xpath.setNamespaceURIs(NAMESPACE_MAP);

        final ImmutableMap<String, ExtractedDetail> emptyMap = ImmutableMap.of();

        final Element tableElement = (Element) xpath.selectSingleNode(macroBodyDoc);
        if (tableElement == null)
            return emptyMap;

        @SuppressWarnings("unchecked")
        final List<Element> rowElements = tableElement.elements("tr");
        if (rowElements == null)
            return emptyMap;

        return loadDetailPairsFromTableRows(rowElements);
    }

    private ImmutableMap<String, ExtractedDetail> loadDetailPairsFromTableRows(List<Element> rowElements) throws IOException
    {
        // The table might be in horizontal or vertical orientation, and with or without table-header (th) cells.
        boolean firstRowIsThs = false;
        List<Element> keyElements = Lists.newArrayList();
        List<Element> valueElements = Lists.newArrayList();

        for (Element rowElem : rowElements)
        {
            @SuppressWarnings("unchecked")
            final List<Element> tds = rowElem.elements("td");
            @SuppressWarnings("unchecked")
            final List<Element> ths = rowElem.elements("th");

            if (!tds.isEmpty())
            {
                if (firstRowIsThs)
                {
                    // The tds in this row are the values corresponding to the keys in the previous header row.
                    // Copy them in and then break - we aren't interested in any subsequent rows.
                    valueElements = Lists.newArrayList(tds);
                    break;
                }

                if (!ths.isEmpty())
                {
                    // New table markup with header as first column.
                    keyElements.add(ths.get(0));
                    valueElements.add(tds.get(0));
                }
                else
                {
                    // Old markup with tds only - keys on left, values on right.
                    keyElements.add(tds.get(0));
                    valueElements.add(tds.size() > 1 ? tds.get(1) : null);
                }
            }
            else if (!ths.isEmpty())
            {
                // New table markup with header as first row - keyElements *are* th elements in this row
                keyElements = Lists.newArrayList(ths);
                firstRowIsThs = true;
            }
        }

        final Map<String, ExtractedDetail> results = new HashMap<String, ExtractedDetail>();

        // Now that we know the key and value element lists we can match them up and create String pairs.
        // Note that the lists might contain nulls, so getInnerHtml needs to be sm4rt.
        for (int i = 0; i < keyElements.size(); i++)
        {
            final Element keyElement = keyElements.get(i);
            final Element valueElement = (valueElements.size() > i) ? valueElements.get(i) : null;

            String key = getKeyText(keyElement);
            if (!results.containsKey(key))
            {
                results.put(key, new ExtractedDetail(getInnerHtml(valueElement), getInnerHtml(keyElement)));
            }
        }

        return ImmutableMap.copyOf(results);
    }

    private String getKeyText(Element element) throws IOException
    {
        if (element == null)
            return "";

        String key;
        if (element.isTextOnly())
        {
            key = element.getText();
        }
        else
        {
            Writer stringWriter = new StringWriter();

            @SuppressWarnings("unchecked")
            Iterator<Node> it = element.nodeIterator();
            while (it.hasNext())
            {
                Node node = it.next();
                if (node instanceof DefaultElement)
                {
                    Element defaultElement = (DefaultElement) node;
                    stringWriter.append(defaultElement.getStringValue());
                }
                else
                    stringWriter.append(node.getText());
            }
            key = stringWriter.toString();
        }

        return StringUtils.remove(StringEscapeUtils.escapeHtml(key), "&nbsp;");
    }

    private String getInnerHtml(Element element) throws IOException
    {
        if (element == null)
            return "";

        /* Why not just element.write(stringWriter), you may be wondering? Because...
             a. You get also the element tags (like "<th>Element Text</th>") and...
            b. Doesn't work for some elements

           NOTE: with the current SAXParser, it is impossible to remove the namespaces from the output
           of node.write - decided to leave it for now.
        */
        if (element.isTextOnly())
        {
            return StringEscapeUtils.escapeHtml(element.getText());
        }
        else
        {
            Writer stringWriter = new StringWriter();

            @SuppressWarnings("unchecked")
            Iterator<Node> it = element.nodeIterator();
            while (it.hasNext())
            {
                Node node = it.next();
                node.write(stringWriter);
            }
            return stringWriter.toString();
        }

    }

    private org.dom4j.Document getMacroBodyDocument(final String macroBodyXhtml)
        throws DocumentException, UnsupportedEncodingException, ParserConfigurationException, SAXException
    {
        StringBuilder builder = new StringBuilder();

        // Add internal DTD declaration to allow support for entities in attribute vales.
        builder.append("<!DOCTYPE xml>").append("<xml");

        for (Namespace namespace : XhtmlConstants.STORAGE_NAMESPACES)
        {
            builder.append(" xmlns");
            if (!namespace.isDefaultNamespace())
                builder.append(":").append(namespace.getPrefix());

            builder.append("=\"").append(namespace.getUri()).append("\"");
        }

        builder.append(">")
                .append(macroBodyXhtml)
                .append("</xml>");
        final ByteArrayInputStream is = new ByteArrayInputStream(builder.toString().getBytes(CHARSET_UTF8));

        SAXParserFactory factory = SAXParserFactory.newInstance();

        javax.xml.parsers.SAXParser saxParser = factory.newSAXParser();
        XMLReader xmlReader = saxParser.getXMLReader();
        SAXReader saxReader = new SAXReader(xmlReader, false);
        return saxReader.read(is);
    }
}
