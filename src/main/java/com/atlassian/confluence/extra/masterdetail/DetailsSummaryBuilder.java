package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.bonnie.Searchable;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.Addressable;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.masterdetail.entities.DetailLine;
import com.atlassian.confluence.extra.masterdetail.entities.PaginatedDetailLines;
import com.atlassian.confluence.extra.masterdetail.rest.ResourceErrorType;
import com.atlassian.confluence.extra.masterdetail.rest.ResourceException;
import com.atlassian.confluence.like.LikeManager;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.CommentManager;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringEscapeUtils;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.apache.commons.lang.text.StrTokenizer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.xml.stream.XMLStreamException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import static com.atlassian.confluence.xhtml.api.MacroDefinitionMarshallingStrategy.MARSHALL_MACRO;
import static com.google.common.collect.Lists.newArrayListWithCapacity;
import static com.google.common.collect.Sets.newLinkedHashSet;
import static org.apache.commons.lang.StringUtils.isNotBlank;
import static org.apache.commons.lang.StringUtils.trim;

public class DetailsSummaryBuilder
{
    private static final Logger log = LoggerFactory.getLogger(DetailsSummaryBuilder.class);

    private final XhtmlContent xhtmlContent;
    private final CommentManager commentManager;
    private final LikeManager likeManager;
    private final CachingDetailsManager cachingDetailsManager;

    public DetailsSummaryBuilder(final XhtmlContent xhtmlContent,
        final CommentManager commentManager,
        final LikeManager likeManager,
        final CachingDetailsManager cachingDetailsManager)
    {
        this.xhtmlContent = xhtmlContent;
        this.commentManager = commentManager;
        this.likeManager = likeManager;
        this.cachingDetailsManager = cachingDetailsManager;
    }

    /**
     * Calculates the unique set of headings based on all the details provided
     * @param detailLines a collection of ExtractedDetails to calculate the headings from
     * @return a list of DetailsHeadings in alphabetical order
     */
    private List<DetailsHeading> buildHeadingsFromDetails(Collection<ExtractedDetails> detailLines)
    {
        final TreeMap<String, String> treeMap = new TreeMap<String, String>();
        for (ExtractedDetails detailLine : detailLines)
        {
            if (!detailLine.getDetails().isEmpty())
                for (String headingText : detailLine.getDetails().keySet())
                {
                    if (treeMap.get(headingText) == null)
                    {
                        try
                        {
                            String renderedValue = headingText;
                            DefaultConversionContext  context = new DefaultConversionContext(detailLine.getContent().toPageContext());
                            String headingStorageFormat = detailLine.getDetails().get(headingText).getHeadingStorageFormat();

                            if (StringUtils.isNotBlank(headingStorageFormat))
                                renderedValue = xhtmlContent.convertStorageToView(headingStorageFormat, context);

                            treeMap.put(headingText, renderedValue);
                        }
                        catch (XMLStreamException e)
                        {
                            log.error("Cannot render xhtml content for heading in page properties macro:" + headingText, e);
                        }
                        catch (XhtmlException e)
                        {
                            log.error("Cannot render xhtml content for heading in page properties macro:" + headingText, e);
                        }
                    }
                }
        }

        List<DetailsHeading> result = new ArrayList<DetailsHeading>(treeMap.size());
        for (String key: treeMap.keySet())
        {
            result.add(new DetailsHeading(key, treeMap.get(key)));
        }
        return result;
    }

    /**
     * Note that because it is user input, the rendered heading must be escaped
     * @param headingsString the user input heading string
     * @return a list of DetailsHeadings in order provided by the headingString
     */
    private List<DetailsHeading> buildHeadingsFromParameter(String headingsString)
    {
        final Set<DetailsHeading> headings = newLinkedHashSet();
        final StrTokenizer tokenizer = StrTokenizer.getCSVInstance(headingsString);
        while (tokenizer.hasNext())
        {
            final String heading = tokenizer.nextToken();
            if (isNotBlank(heading))
            {
                String escapedHeading = StringEscapeUtils.escapeHtml(heading);
                headings.add(new DetailsHeading(escapedHeading, escapedHeading));
            }
        }
        return new ArrayList<DetailsHeading>(headings);
    }

    private List<ExtractedDetails> getDetailsFromContent(final List<ContentEntityObject> content, String detailsId)
    {
        final List<ExtractedDetails> detailLines = newArrayListWithCapacity(content.size());

        for (final ContentEntityObject entity : content)
        {
            long id = entity.getId();
            ImmutableMap<String, ImmutableMap<String, ExtractedDetail>> detailsMap = cachingDetailsManager.get(id);
            if (detailsMap == null)
            {
                // We need to marshall non-detail-macros encountered in the body of the detail macro, so that these macros
                // can be rendered in the table of detail lines.
                final DefaultConversionContext subContext = new DefaultConversionContext(entity.toPageContext());
                final DetailsMacroBodyHandler macroBodyHandler = new DetailsMacroBodyHandler();

                try
                {
                    xhtmlContent.handleMacroDefinitions(entity.getBodyAsString(), subContext, macroBodyHandler, MARSHALL_MACRO);
                }
                catch (XhtmlException e)
                {
                    log.error("Cannot extract page properties from content with id: " + entity.getIdAsString(), e);
                }
                detailsMap = macroBodyHandler.getDetails();
                cachingDetailsManager.put(id, detailsMap);
            }

            Map<String, ExtractedDetail> result;
            if (StringUtils.isBlank(detailsId)) // if ID is not specified, we get all the properties from the page
            {
                result = new HashMap<String, ExtractedDetail>();
                for (Iterator iterator = detailsMap.values().iterator(); iterator.hasNext(); )
                {
                    Map<String, ExtractedDetail> next = (Map<String, ExtractedDetail>) iterator.next();
                    result.putAll(next);
                }

            }
            else
            {
                result = detailsMap.get(detailsId);
            }

            if (result != null && result.size() > 0)
                detailLines.add(new ExtractedDetails(entity, result));
        }

        return detailLines;
    }

    public void fillWithCountsAndLink(List<DetailLine> detailLines, boolean countComments, boolean countLikes)
    {
        Map<Searchable, Integer> commentsCounts = null;
        Map<Searchable, Integer> likesCounts = null;
        if (countComments || countLikes)
        {
            final List<Searchable> contents = Lists.transform(detailLines, new Function<DetailLine, Searchable>() {
                public Searchable apply(DetailLine detailLine)
                {
                    return detailLine.getContent();
                }
            });

            commentsCounts = countComments ? commentManager.countComments(contents) : null;
            likesCounts = countLikes ? likeManager.countLikes(contents) : null;
        }

        for (final DetailLine detailLine : detailLines)
        {
            fillLineTitleLink(detailLine);
            if (countComments)
            {
                detailLine.setCommentsCount(commentsCounts.get(detailLine.getContent()));
            }
            if (countLikes)
            {
                detailLine.setLikesCount(likesCounts.get(detailLine.getContent()));
            }
        }
    }

    private void fillLineTitleLink(final DetailLine detailLine)
    {
        final Addressable content = detailLine.getContent();
        final String type = content.getType();

        if (Comment.CONTENT_TYPE.equals(type))
        {
            Comment comment = (Comment) content;

            ContentEntityObject owner = comment.getOwner();

            detailLine.setTitle(owner.getDisplayTitle());
            detailLine.setSubTitle(comment.getDisplayTitle());

            detailLine.setRelativeLink(owner.getUrlPath());
            detailLine.setSubRelativeLink(content.getUrlPath());
        }
        else
        {
            detailLine.setTitle(content.getDisplayTitle());
            detailLine.setRelativeLink(content.getUrlPath());
        }
    }

    /*
         * If a comma-separated list of headings is present as a "headings" parameters, then split and sanitize that list.
         * Else walk all detail lines and return the keys. Note it is important to keep ordering of the headings so it
         * must return a list.
         */
    private List<DetailsHeading> getHeadings(String headingsString, final Collection<ExtractedDetails> detailLines)
    {
        headingsString = trim(headingsString);
        if (isNotBlank(headingsString))
        {
            return buildHeadingsFromParameter(headingsString);
        }

        return buildHeadingsFromDetails(detailLines);
    }

    public PaginatedDetailLines getPaginatedDetailLines(int pageSize, final int currentPage,
                                                        final boolean countComments, final boolean countLikes,
                                                        final String headingsString,
                                                        final String sortBy,
                                                        final boolean reverseSort,
                                                        final List<ContentEntityObject> content,
                                                        final String id)
    {
        if (pageSize > DetailsSummaryMacro.MAX_PAGESIZE)
            pageSize = DetailsSummaryMacro.MAX_PAGESIZE;

        // First bailout, just in case your requested page is *way* too large (content.size() is >= final size)
        int totalPages = getTotalPages(content.size(), pageSize);
        checkPageBounds(totalPages, currentPage);

        final List<ExtractedDetails> totalLines = getDetailsFromContent(content, id);
        if (totalLines.isEmpty())
            return new PaginatedDetailLines(currentPage, totalPages, Collections.<DetailLine>emptyList(), Collections.<String>emptyList());

        final PaginatedDetailLines result = new PaginatedDetailLines();
        final List<DetailsHeading> headings = getHeadings(headingsString, totalLines);

        if (StringUtils.isNotBlank(sortBy))
        {
            Collections.sort(totalLines, new ExtractedDetailsComparator(StringEscapeUtils.escapeHtml(sortBy), reverseSort));
        }

        // Reassign totalPages, as the first number is likely to be incorrect
        totalPages = getTotalPages(totalLines.size(), pageSize);
        checkPageBounds(totalPages, currentPage);

        final List<ExtractedDetails> pagedDetailLines = Lists.partition(totalLines, pageSize).get(currentPage);
        final List<DetailLine> renderedLines = new ArrayList<DetailLine>(pagedDetailLines.size());

        for (ExtractedDetails line : pagedDetailLines)
        {
            final List<String> cells = new ArrayList<String>(headings.size());
            final ContentEntityObject lineContent = line.getContent();
            final DefaultConversionContext subContext = new DefaultConversionContext(lineContent.toPageContext());
            final Map<String, ExtractedDetail> lineDetails = line.getDetails();
            for (DetailsHeading heading : headings)
            {
                ExtractedDetail extractedDetail = lineDetails.get(heading.getHeading());
                boolean added = false;
                if (extractedDetail != null && StringUtils.isNotBlank(extractedDetail.getDetailStorageFormat()))
                {
                    final String value = extractedDetail.getDetailStorageFormat();
                    try
                    {
                        final String renderedValue = xhtmlContent.convertStorageToView(value, subContext);
                        cells.add(renderedValue);
                        added = true;
                    }
                    catch (XMLStreamException e)
                    {
                        log.error("Cannot render xhtml content for page properties macro:" + value, e);
                    }
                    catch (XhtmlException e)
                    {
                        log.error("Cannot render xhtml content for page properties macro:" + value, e);
                    }
                }
                if (!added)
                    cells.add("");
            }
            renderedLines.add(new DetailLine(lineContent, cells));
        }

        fillWithCountsAndLink(renderedLines, countComments, countLikes);

        final List<String> renderedHeadings = new ArrayList<String>(headings.size());
        for (DetailsHeading heading : headings)
            renderedHeadings.add(heading.getRenderedHeading());

        result.setHeadings(renderedHeadings);
        result.setCurrentPage(currentPage);
        result.setTotalPages(totalPages);
        result.setDetailLines(renderedLines);

        return result;
    }

    private static int getTotalPages(int total, int pageSize)
    {
        return (int) Math.ceil((double)total / (double)pageSize);
    }

    private static void checkPageBounds(int totalPages, int currentPage)
    {
        if (currentPage >= totalPages && totalPages != 0)
            throw new ResourceException("Requested page is outside bounds", Response.Status.BAD_REQUEST, ResourceErrorType.INVALID_INDEX_PAGE, currentPage);
    }
}
