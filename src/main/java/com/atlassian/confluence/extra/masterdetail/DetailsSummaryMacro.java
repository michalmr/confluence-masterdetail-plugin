package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.ConversionContextOutputType;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.extra.masterdetail.entities.PaginatedDetailLines;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Draft;
import com.atlassian.confluence.plugins.createcontent.api.services.CreateButtonService;
import com.atlassian.confluence.renderer.template.TemplateRenderer;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugin.ModuleCompleteKey;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import java.util.List;
import java.util.Map;

import static com.atlassian.confluence.macro.Macro.BodyType.NONE;
import static com.atlassian.renderer.v2.RenderMode.NO_RENDER;
import static com.atlassian.renderer.v2.RenderUtils.blockError;
import static com.google.common.collect.Lists.newArrayList;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * This implements both the old-style {@link com.atlassian.renderer.v2.macro.Macro} and the newer
 * {@link Macro} interface.
 */
public class DetailsSummaryMacro extends BaseMacro implements Macro
{
    private static final Logger LOG = getLogger(DetailsSummaryMacro.class);

    public static final String PARAM_ID = "id";
    private static final String PARAM_SHOW_COMMENTS_COUNT = "showCommentsCount";
    private static final String PARAM_SHOW_LIKES_COUNT = "showLikesCount";
    private static final String PARAM_PAGE_SIZE = "pageSize";
    private static final String PARAM_SORT_BY = "sortBy";
    private static final String PARAM_REVERSE_SORT = "reverseSort";

    private static final String TEMPLATE_PARAM_TOTAL_PAGES = "totalPages";
    private static final String TEMPLATE_PARAM_CURRENT_PAGE = "currentPage";
    private static final String TEMPLATE_PARAM_PAGE_SIZE = "pageSize";
    private static final String TEMPLATE_PARAM_LABEL = "label";
    private static final String TEMPLATE_PARAM_HEADINGS = "headings";
    private static final String TEMPLATE_PARAM_SPACES = "spaces";
    private static final String TEMPLATE_PARAM_DETAILS = "details";

    public static final int DEFAULT_PAGE_SIZE = 30;
    public static final int MAX_PAGESIZE = 1000;
    public static final int PARAM_ID_MAX_LENGTH = 256;

    private final XhtmlContent xhtmlContent;
    private final WebResourceManager webResourceManager;
    private final I18NBeanFactory i18NBeanFactory;
    private final LocaleManager localeManager;
    private final ContentRetriever contentRetriever;
    private final DetailsSummaryBuilder detailsSummaryBuilder;
    private final PermissionManager permissionManager;
    private final SpaceManager spaceManager;
    private final SettingsManager settingsManager;
    private final TemplateRenderer templateRenderer;
    private final CreateButtonService createButtonService;

    public DetailsSummaryMacro(LocaleManager localeManager, I18NBeanFactory i18NBeanFactory,
        XhtmlContent xhtmlContent,
        SpaceManager spaceManager, WebResourceManager webResourceManager,
        CreateButtonService createButtonService, ContentRetriever contentRetriever,
        DetailsSummaryBuilder detailsSummaryBuilder, SettingsManager settingsManager,
        TemplateRenderer templateRenderer, PermissionManager permissionManager)
    {
        this.xhtmlContent = xhtmlContent;
        this.webResourceManager = webResourceManager;
        this.templateRenderer = templateRenderer;
        this.createButtonService = createButtonService;
        this.contentRetriever = contentRetriever;
        this.i18NBeanFactory = i18NBeanFactory;
        this.localeManager = localeManager;
        this.detailsSummaryBuilder = detailsSummaryBuilder;
        this.permissionManager = permissionManager;
        this.spaceManager = spaceManager;
        this.settingsManager = settingsManager;
    }

    /**
     * From the old {@link com.atlassian.renderer.v2.macro.Macro} interface.
     * Converts the old wiki markup body into storage format XHTML, then delegates to the newer interface.
     */
    @Override
    public String execute(Map macroParameters, String bodyWikiMarkup, RenderContext renderContext)
    {
        // TODO SHouldn't we do something with this list?
        final List<RuntimeException> wikiMarkupToStorageConversionErrors = newArrayList();
        final ConversionContext conversionContext = new DefaultConversionContext(renderContext);

        final String storageFormatBody = xhtmlContent.convertWikiToStorage(
                bodyWikiMarkup,
                conversionContext,
                wikiMarkupToStorageConversionErrors
        );

        return execute(macroParameters, storageFormatBody, conversionContext);
    }

    /**
     * From the  {@link com.atlassian.confluence.macro.Macro} interface
     */
    @Override
    public String execute(final Map<String, String> macroParameters, final String bodyStorage, final ConversionContext conversionContext)
    {
        try
        {
            validateContextOwner(conversionContext);

            // do not paginate results summary when rendering to pdf
            boolean contentExported = ConversionContextOutputType.PDF.value().equals(conversionContext.getOutputType());

            String spaces = macroParameters.get("spaces");
            final String spaceKey = conversionContext.getSpaceKey();
            final List<String> spaceKeys = contentRetriever.getSpaceKeysFromDelimitedString(spaces);

            final List<ContentEntityObject> content = contentRetriever.getContentWithMetaData(macroParameters.get("label"), spaceKeys, spaceKey);
            return renderContentSummary(macroParameters, content, spaceKeys, spaceKey, contentExported);
        }
        catch (MacroExecutionException e)
        {
            // Couldn't determine contents for some reason - render that reason for the user.
            return blockError(e.getMessage(), "");
        }
    }

    private String renderContentSummary(final Map<String, String> macroParameters, final List<ContentEntityObject> content,
        final List<String> spaceKeys, final String spaceKey, final boolean doNotPaginate) throws MacroExecutionException
    {
        int currentPage = 0;
        int pageSize = doNotPaginate ? MAX_PAGESIZE : getIntParam(macroParameters, PARAM_PAGE_SIZE, DEFAULT_PAGE_SIZE);
        if (pageSize <= 0)
            throw new MacroExecutionException(getI18NBean().getText("detailssummary.error.pagesize.invalid"));
        else if(pageSize > MAX_PAGESIZE)
            pageSize = MAX_PAGESIZE;

        String id = macroParameters.get(DetailsSummaryMacro.PARAM_ID);
        if (id != null && id.length() > PARAM_ID_MAX_LENGTH)
            throw new MacroExecutionException(getI18NBean().getText("details.error.id.length"));

        boolean countComments = getBooleanParam(macroParameters, PARAM_SHOW_COMMENTS_COUNT);
        boolean countLikes = getBooleanParam(macroParameters, PARAM_SHOW_LIKES_COUNT);
        String sortBy = macroParameters.get(PARAM_SORT_BY);
        boolean reverseSort = getBooleanParam(macroParameters, PARAM_REVERSE_SORT);
        PaginatedDetailLines paginatedDetailLines = detailsSummaryBuilder.getPaginatedDetailLines(
            pageSize, currentPage, countComments, countLikes, macroParameters.get("headings"), sortBy, reverseSort,
            content, id
        );

        final Map<String, Object> contextMap = getTemplateContext(macroParameters, paginatedDetailLines, pageSize, spaceKeys, spaceKey, sortBy, reverseSort);

        return renderWithSummaryTemplate(contextMap);
    }

    private Map<String, Object> getTemplateContext(Map<String, String> macroParameters, PaginatedDetailLines paginatedDetailLines,
        int pageSize, final List<String> spaceKeys, String spaceKey, String sortBy, boolean reverseSort)
    {
        final Map<String, Object> contextMap = Maps.newHashMap();

        String firstColumnParam = macroParameters.get("firstcolumn");
        String firstColumnHeading = isBlank(firstColumnParam) ? getI18NBean().getText("detailssummary.heading.title") : firstColumnParam;

        contextMap.put("firstColumnHeading", firstColumnHeading);
        contextMap.put(TEMPLATE_PARAM_LABEL, macroParameters.get("label"));
        contextMap.put(TEMPLATE_PARAM_HEADINGS, paginatedDetailLines.getHeadings());
        contextMap.put(TEMPLATE_PARAM_SPACES, spaceKeys);
        contextMap.put(TEMPLATE_PARAM_DETAILS, paginatedDetailLines.getDetailLines());
        contextMap.put(PARAM_SORT_BY, sortBy);
        contextMap.put(PARAM_ID, macroParameters.get(PARAM_ID));
        contextMap.put(PARAM_REVERSE_SORT, reverseSort);
        contextMap.put("analyticsKey", macroParameters.get("analytics-key"));
        contextMap.put("showCommentsCount", getBooleanParam(macroParameters, PARAM_SHOW_COMMENTS_COUNT));
        contextMap.put("showLikesCount", getBooleanParam(macroParameters, PARAM_SHOW_LIKES_COUNT));

        contextMap.put(TEMPLATE_PARAM_TOTAL_PAGES, paginatedDetailLines.getTotalPages());
        contextMap.put(TEMPLATE_PARAM_CURRENT_PAGE, paginatedDetailLines.getCurrentPage());
        contextMap.put(TEMPLATE_PARAM_PAGE_SIZE, pageSize);

        // for blank experience of blueprint, only activated on the presence of extra parameter
        String blueprintModuleCompleteKey = macroParameters.get("blueprintModuleCompleteKey");
        if (paginatedDetailLines.getDetailLines().isEmpty() && StringUtils.isNotBlank(blueprintModuleCompleteKey))
        {
            addBlankExperienceToContext(macroParameters, getCurrentSpace(spaceKey), contextMap); //todo check if blank exp space is correct
        }

        return contextMap;
    }
    
    private void addBlankExperienceToContext(Map<String, String> macroParameters, Space space, Map<String, Object> context)
    {
        String blueprintModuleCompleteKey = macroParameters.get("blueprintModuleCompleteKey");
        String contentBlueprintId = macroParameters.get("contentBlueprintId");
        if (isBlank(blueprintModuleCompleteKey) && isBlank(contentBlueprintId))
            // For now, only add a "Blank Experience" when there is a Blueprint in play.
            return;

        context.put("blankTitle", macroParameters.get("blankTitle"));
        context.put("blankDescription", macroParameters.get("blankDescription"));

        if (blueprintModuleCompleteKey == null)
        {
            blueprintModuleCompleteKey = "";
        }
        else
        {
            // blueprintModuleKey is just used in the Velocity template as an element class, and may be null.
            String moduleKey = getModuleKey(blueprintModuleCompleteKey);
            context.put("blueprintModuleKey", moduleKey);
        }

        if (contentBlueprintId == null)
            contentBlueprintId = "";

        String buttonLabel = macroParameters.get("createButtonLabel");

        String createFromTemplateHtml = createButtonService.renderBlueprintButton(space, contentBlueprintId, blueprintModuleCompleteKey, buttonLabel, null);

        context.put("createFromTemplateHtml", createFromTemplateHtml);
    }

    private String getModuleKey(String blueprintModuleCompleteKey)
    {
        return (new ModuleCompleteKey(blueprintModuleCompleteKey)).getModuleKey();
    }

    private void validateContextOwner(final ConversionContext conversionContext) throws MacroExecutionException
    {
        final ContentEntityObject owner = conversionContext.getEntity();
        if (!(owner instanceof SpaceContentEntityObject || owner instanceof Draft))
            throw new MacroExecutionException(getI18NBean().getText("detailssummary.error.must.be.inside.space"));
    }

    protected String renderWithSummaryTemplate(final Map<String, Object> contextMap)
    {
        final String template = contextMap.containsKey("blankTitle") ? "detailsSummaryBlank" : "detailsSummary";

        webResourceManager.requireResource(DetailsMacro.MASTERDETAIL_RESOURCES);
        StringBuilder buf = new StringBuilder();
        templateRenderer.renderTo(buf, DetailsMacro.MASTERDETAIL_RESOURCES, "Confluence.Templates.Macro.MasterDetail." + template + ".soy", contextMap);
        return buf.toString();
    }

    /**
     * From the old {@link com.atlassian.renderer.v2.macro.Macro} interface
     */
    @Override
    public RenderMode getBodyRenderMode()
    {
        return NO_RENDER;
    }

    /**
     * From the old {@link com.atlassian.renderer.v2.macro.Macro} interface
     */
    @Override
    public boolean hasBody()
    {
        return false;
    }

    /**
     * From the old {@link com.atlassian.renderer.v2.macro.Macro} interface
     */
    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context)
    {
        return TokenType.BLOCK;
    }

    /**
     * From the  {@link com.atlassian.confluence.macro.Macro} interface
     */
    @Override
    public BodyType getBodyType()
    {
        return NONE;
    }

    /**
     * From the  {@link com.atlassian.confluence.macro.Macro} interface
     */
    @Override
    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }

    private boolean getBooleanParam(Map<String, String> macroParams, String paramName)
    {
        return Boolean.parseBoolean(macroParams.get(paramName));
    }

    private int getIntParam(Map<String, String> macroParams, String paramName, int defaultValue)
    {
        try
        {
            return Integer.parseInt(macroParams.get(paramName));
        }
        catch (Exception ex)
        {
            return defaultValue;
        }
    }

    private I18NBean getI18NBean()
    {
        return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.get()));
    }

    private Space getCurrentSpace(String spaceKey)
    {
        return spaceManager.getSpace(spaceKey);
    }
}
