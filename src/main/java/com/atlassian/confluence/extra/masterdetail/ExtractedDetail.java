package com.atlassian.confluence.extra.masterdetail;

import java.io.Serializable;

public class ExtractedDetail implements Serializable
{
    private final String detailStorageFormat;
    private final String headingStorageFormat;

    public ExtractedDetail(String detailStorageFormat, String headingStorageFormat)
    {
        this.detailStorageFormat = detailStorageFormat;
        this.headingStorageFormat = headingStorageFormat;
    }

    public String getDetailStorageFormat()
    {
        return detailStorageFormat;
    }

    public String getHeadingStorageFormat()
    {
        return headingStorageFormat;
    }
}
