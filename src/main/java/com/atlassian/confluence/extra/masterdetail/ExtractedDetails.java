package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.core.ContentEntityObject;

import java.util.Map;

/**
 * Simple object to represent the extracted details for an object
 */
public class ExtractedDetails
{
    final private ContentEntityObject content;
    private final Map<String, ExtractedDetail> details;

    public ExtractedDetails(ContentEntityObject content, Map<String, ExtractedDetail> details)
    {
        this.content = content;
        this.details = details;
    }

    public String getTitle()
    {
        if (content != null)
            return content.getTitle();

        return null;
    }

    public ContentEntityObject getContent()
    {
        return content;
    }

    /**
     * A map of details keyed by heading text to ExtractedDetail. Note that headings will be just plain text and not
     * include html markup. You can get the heading storage format from the ExtractedDetail.
     */
    public Map<String, ExtractedDetail> getDetails()
    {
        return details;
    }

    public String getDetailStorageFormat(String heading)
    {
        if (details == null)
            return "";

        ExtractedDetail extractedDetail = details.get(heading);
        return extractedDetail == null ? "" : extractedDetail.getDetailStorageFormat();

    }
}
