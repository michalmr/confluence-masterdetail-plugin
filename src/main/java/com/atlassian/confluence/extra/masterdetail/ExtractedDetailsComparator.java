package com.atlassian.confluence.extra.masterdetail;

import java.util.Comparator;

public class ExtractedDetailsComparator implements Comparator<ExtractedDetails>
{
    final private String key;
    final private boolean reverseSort;

    public ExtractedDetailsComparator(String key, boolean reverseSort)
    {
        this.key = key;
        this.reverseSort = reverseSort;
    }

    @Override
    public int compare(ExtractedDetails first, ExtractedDetails second)
    {
        //special key not in the details map
        boolean titleKey = ("title".equalsIgnoreCase(key));

        String firstValue = first == null ? "" : (titleKey ? first.getTitle() : first.getDetailStorageFormat(key));
        String secondValue = second == null ? "": (titleKey ? second.getTitle() :  second.getDetailStorageFormat(key));


        if (firstValue == null) firstValue = "";
        if (secondValue == null) secondValue = "";

        return reverseSort ? secondValue.compareToIgnoreCase(firstValue) : firstValue.compareToIgnoreCase(secondValue);
    }
}
