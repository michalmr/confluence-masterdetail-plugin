package com.atlassian.confluence.extra.masterdetail.entities;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * Represents a paginated details summary page.
 *
 * NOTE: Everything put into this class must be escaped and html safe.
 */
public class PaginatedDetailLines
{
    @JsonProperty
    private int currentPage;
    @JsonProperty
    private int totalPages;
    @JsonProperty
    private List<DetailLine> detailLines;
    @JsonProperty
    private List<String> headings;

    // JAXB
    public PaginatedDetailLines() {}

    public PaginatedDetailLines(int currentPage, int totalPages, List<DetailLine> detailLines, List<String> headings)
    {
        this.currentPage = currentPage;
        this.totalPages = totalPages;
        this.detailLines = detailLines;
        this.headings = headings;
    }

    public int getCurrentPage()
    {
        return currentPage;
    }

    public void setCurrentPage(final int currentPage)
    {
        this.currentPage = currentPage;
    }

    public int getTotalPages()
    {
        return totalPages;
    }

    public void setTotalPages(final int totalPages)
    {
        this.totalPages = totalPages;
    }

    public List<DetailLine> getDetailLines()
    {
        return detailLines;
    }

    public void setDetailLines(final List<DetailLine> detailLines)
    {
        this.detailLines = detailLines;
    }

    public void setHeadings(final List<String> headings)
    {
        this.headings = headings;
    }

    public List<String> getHeadings()
    {
        return headings;
    }
}
