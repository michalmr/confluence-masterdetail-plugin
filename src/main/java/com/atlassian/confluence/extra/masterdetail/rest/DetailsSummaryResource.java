package com.atlassian.confluence.extra.masterdetail.rest;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.masterdetail.CachingDetailsManager;
import com.atlassian.confluence.extra.masterdetail.ContentRetriever;
import com.atlassian.confluence.extra.masterdetail.DetailsSummaryBuilder;
import com.atlassian.confluence.extra.masterdetail.DetailsSummaryMacro;
import com.atlassian.confluence.extra.masterdetail.entities.PaginatedDetailLines;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.like.LikeManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.CommentManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("detailssummary")
public class DetailsSummaryResource
{
    private final DetailsSummaryBuilder detailsSummaryBuilder;
    private final ContentRetriever contentRetriever;

    public DetailsSummaryResource(final LocaleManager localeManager,
                                  final I18NBeanFactory i18NBeanFactory,
                                  final LabelManager labelManager,
                                  final XhtmlContent xhtmlContent,
                                  final SpaceManager spaceManager,
                                  final PermissionManager permissionManager,
                                  final CommentManager commentManager,
                                  final LikeManager likeManager,
                                  final CachingDetailsManager cachingDetailsManager)
    {
        this.contentRetriever = new ContentRetriever(labelManager, spaceManager, permissionManager, localeManager, i18NBeanFactory);
        this.detailsSummaryBuilder = new DetailsSummaryBuilder(xhtmlContent, commentManager, likeManager, cachingDetailsManager);
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public PaginatedDetailLines get(@QueryParam("label") final String label,
                                    @QueryParam("id") final String id,
                                    @QueryParam("sortBy") final String sortBy,
                                    @QueryParam("reverseSort") @DefaultValue("false") final boolean reverseSort,
                                    @QueryParam("spaces") final String spaces,
                                    @QueryParam("spaceKey") final String spaceKey,
                                    @QueryParam("pageSize") @DefaultValue(""+DetailsSummaryMacro.DEFAULT_PAGE_SIZE) final int pageSize,
                                    @QueryParam("pageIndex") @DefaultValue("0") final int currentPage,
                                    @QueryParam("countComments") @DefaultValue("false") final boolean countComments,
                                    @QueryParam("countLikes") @DefaultValue("false") final boolean countLikes,
                                    @QueryParam("headings") final String headings)
    {
        if (StringUtils.isEmpty(label))
            throw new ResourceException("'label' parameter is required", Response.Status.BAD_REQUEST, ResourceErrorType.PARAMETER_MISSING, "label");

        if (StringUtils.isEmpty(spaces) && StringUtils.isEmpty(spaceKey))
            throw new ResourceException("Either 'spaces' or 'spaceKey' parameter is required", Response.Status.BAD_REQUEST, ResourceErrorType.PARAMETER_MISSING, "spaces,spaceKey");

        if (pageSize <= 0)
            throw new ResourceException("Requested page size is not valid", Response.Status.BAD_REQUEST, ResourceErrorType.PARAMETER_INVALID, "pageSize");

        if (currentPage < 0)
            throw new ResourceException("Requested page index is not valid", Response.Status.BAD_REQUEST, ResourceErrorType.PARAMETER_INVALID, "pageIndex");

        try
        {
            final List<ContentEntityObject> content = contentRetriever.getContentWithMetaData(label,
                contentRetriever.getSpaceKeysFromDelimitedString(spaces), spaceKey);
            return detailsSummaryBuilder.getPaginatedDetailLines(pageSize, currentPage, countComments, countLikes, headings, sortBy, reverseSort, content, id);
        }
        catch (MacroExecutionException e)
        {
            throw new ResourceException(e, Response.Status.INTERNAL_SERVER_ERROR, ResourceErrorType.RENDERING_MACRO);
        }
    }
}
