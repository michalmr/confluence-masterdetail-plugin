package com.atlassian.confluence.extra.masterdetail.rest;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

// FIXME: Copied from Create Content plugin!
@XmlRootElement
public class ResourceErrorBean
{
    @XmlAttribute
    private int errorCode;
    @XmlAttribute
    private String errorType;
    @XmlElement
    private Object errorData;
    @XmlElement
    private String errorMessage;

    // JAXB
    private ResourceErrorBean() {}

    public ResourceErrorBean(final int errorCode, @Nonnull final String errorType, @Nullable final Object errorData, @Nonnull final String errorMessage)
    {
        this.errorCode = errorCode;
        this.errorType = errorType;
        this.errorData = errorData;
        this.errorMessage = errorMessage;
    }
}
