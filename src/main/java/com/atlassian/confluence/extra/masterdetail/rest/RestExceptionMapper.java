package com.atlassian.confluence.extra.masterdetail.rest;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

// FIXME: Copied from Create Content plugin!
@Provider
public class RestExceptionMapper implements ExceptionMapper<Exception>
{
    @Override
    public Response toResponse(final Exception exception)
    {
        if (exception instanceof WebApplicationException)
        {
            return ((WebApplicationException)exception).getResponse();
        }
        return ResourceException.makeResponse(exception.getMessage(), Response.Status.INTERNAL_SERVER_ERROR, ResourceErrorType.UNKNOWN, null);
    }
}