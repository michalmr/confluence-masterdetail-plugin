package com.atlassian.confluence.extra.masterdetail.upgrade;

import com.atlassian.confluence.extra.masterdetail.CachingDetailsManager;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;

import java.util.Collection;

public class ClearCacheUpgradeTask implements PluginUpgradeTask
{
    private final CachingDetailsManager cachingDetailsManager;

    public ClearCacheUpgradeTask(CachingDetailsManager cachingDetailsManager)
    {
        this.cachingDetailsManager = cachingDetailsManager;
    }

    @Override
    public int getBuildNumber()
    {
        return 2; //update this number whenever the cache structure changes
    }

    @Override
    public String getShortDescription()
    {
        return "Clears the page properties cache";
    }

    @Override
    public Collection<Message> doUpgrade() throws Exception
    {
        cachingDetailsManager.removeAll();
        return null;
    }

    @Override
    public String getPluginKey()
    {
        return "confluence.extra.masterdetail";
    }
}
