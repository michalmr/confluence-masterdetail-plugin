AJS.$(function ($) {

    var removeBraces = function(str) {
        if (str)
            return ("" + str).replace(/[\[\]]/g, "");

        return "";
    };

    _.each($(".metadata-summary-macro").parent(), function(obj) {
        var $table = $(obj).find('table.metadata-summary-macro'),
            showCommentsCount = $table.data('count-comments'),
            showLikesCount = $table.data('count-likes'),
            pageSize = $table.data('page-size'),
            totalPages = $table.data('total-pages'),
            headings = removeBraces($table.data('headings')),
            sortBy = removeBraces($table.data('sort-by')),
            reverseSort = removeBraces($table.data('reverse-sort')),
            spaces = removeBraces($table.data('spaces'));

        // no need to paginate
        if (totalPages <= 1) return;

        Confluence.UI.Components.Pagination.build({
            'scope': $(obj),
            'pageSize': pageSize,
            'totalPages': totalPages,
            'path': "/rest/masterdetail/1.0/detailssummary",
            'data': {
                'spaceKey': AJS.Meta.get('space-key'),
                'label': $table.data('label'),
                'id': $table.data('id'),
                'spaces': spaces,
                'headings': headings,
                'countComments': showCommentsCount,
                'countLikes': showLikesCount,
                'sortBy': sortBy,
                'reverseSort': reverseSort
            },
            'success': function addLine(line, $container) {
                var lineData = {
                    'line':line,
                    'headings':headings.split(','),
                    'showCommentsCount':showCommentsCount,
                    'showLikesCount':showLikesCount
                };
                var renderedLine = Confluence.Templates.Macro.MasterDetail.detailsSummaryLine(lineData);

                $container.append(renderedLine);
                $table.trigger("update"); // fix table header sorting
            }
        });
    });
});