package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.PartialList;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.user.User;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static com.atlassian.confluence.labels.LabelManager.NO_MAX_RESULTS;
import static com.atlassian.confluence.labels.LabelManager.NO_OFFSET;
import static com.google.common.collect.Maps.newHashMap;
import static java.util.Arrays.asList;
import static junit.framework.TestCase.fail;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestContentRetriever
{
    @Mock private LabelManager labelManager;
    @Mock private SpaceManager spaceManager;
    @Mock private PermissionManager permissionManager;
    @Mock private LocaleManager localeManager;
    @Mock private I18NBeanFactory i18NBeanFactory;

    private ContentRetriever contentRetriever;
    private final Map<String, String> macroParameters = newHashMap();
    private final Label myLabel = new Label("mylabel");

    @Before
    public void setup()
    {
        final I18NBean i18NBean = mock(I18NBean.class);
        when(localeManager.getLocale((User) any())).thenReturn(Locale.ENGLISH);
        when(i18NBeanFactory.getI18NBean(Locale.ENGLISH)).thenReturn(i18NBean);

        when(i18NBean.getText(anyString())).thenAnswer(RETURN_FIRST_ARGUMENT_MESSAGE);
        when(i18NBean.getText(anyString(), any(Serializable[].class))).thenAnswer(RETURN_FIRST_ARGUMENT_MESSAGE);

        when(labelManager.getLabel(myLabel)).thenReturn(myLabel);

        contentRetriever = new ContentRetriever(labelManager, spaceManager, permissionManager, localeManager, i18NBeanFactory);
    }

    private Space mockSpace(String spaceKey)
    {
        Space space = mock(Space.class, spaceKey);
        when(spaceManager.getSpace(spaceKey)).thenReturn(space);
        when(space.getKey()).thenReturn(spaceKey);
        return space;
    }

    private Page mockPage(String title)
    {
        Page page = mock(Page.class, title);
        when(page.getTitle()).thenReturn(title);
        when(page.getNameForComparison()).thenReturn(title);
        when(page.getLastModificationDate()).thenReturn(new Date());

        return page;
    }

    @Test
    public void testGetSpaceKeysFromDelimitedString()
    {
        List<String> result = contentRetriever.getSpaceKeysFromDelimitedString("foo,BAR, blah1234, @all");
        assertEquals(Arrays.asList("foo", "BAR", "blah1234", "@all"), result);
    }

    @Test
    public void contentIsFetchedByLabelAndCurrentSpace() throws Exception
    {
        final String spaceKey = "TST";
        mockSpace(spaceKey);

        // When space is not specified as a macro parameter the conversion context space is used
        contentRetriever.getContentWithMetaData(myLabel.getName(), null, spaceKey);

        verify(labelManager).getLabel(myLabel);
        verify(labelManager).getContentInSpacesForAllLabels(NO_OFFSET, NO_MAX_RESULTS, Sets.newHashSet(spaceKey), myLabel);
        verifyNoMoreInteractions(labelManager);
    }

    @Test
    public void contentIsFetchedByLabelAndSingleSpace() throws Exception
    {
        macroParameters.put("label", myLabel.getName());
        macroParameters.put("spaces", "XYZ");

        Space xyz = mockSpace("XYZ");
        when(labelManager.getSpacesContainingContentWithLabel(myLabel)).thenReturn(asList(xyz));

        // When space is specified as a macro parameter the conversion context space is not used
        contentRetriever.getContentWithMetaData(myLabel.getName(), Arrays.asList("XYZ"), "");

        verify(labelManager).getLabel(myLabel);
        verify(labelManager).getContentInSpacesForAllLabels(NO_OFFSET, NO_MAX_RESULTS, Sets.newHashSet("XYZ"), myLabel);
        verifyNoMoreInteractions(labelManager);
    }

    @Test
    public void contentIsFetchedByLabelAndMultipleSpaces() throws Exception
    {
        Space xyz = mockSpace("XYZ");
        Space pzq = mockSpace("PZQ");
        when(labelManager.getSpacesContainingContentWithLabel(myLabel)).thenReturn(asList(xyz, pzq));

        contentRetriever.getContentWithMetaData(myLabel.getName(), Arrays.asList("XYZ", "PZQ"), "");

        verify(labelManager).getLabel(myLabel);
        verify(labelManager).getContentInSpacesForAllLabels(NO_OFFSET, NO_MAX_RESULTS, Sets.newHashSet("XYZ", "PZQ"), myLabel);
        verifyNoMoreInteractions(labelManager);
    }

    @Test
    public void allSpacesKeyTakesPrecendenceOverAllSpaceKeys() throws Exception
    {
        macroParameters.put("label", myLabel.getName());
        macroParameters.put("spaces", "@all, XYZ");

        mockSpace("XYZ");
        Set<String> spaceKeys = Sets.newHashSet();  // note - no XYZ in here
        List<ContentEntityObject> labeledPages = Lists.newArrayList();
        mockLabelManagerGetContentForMyLabel(spaceKeys, labeledPages);

        contentRetriever.getContentWithMetaData(myLabel.getName(), Arrays.asList("XYZ", "@all"), "");

        verify(labelManager).getLabel(myLabel);
        verify(labelManager).getContentInSpacesForAllLabels(NO_OFFSET, NO_MAX_RESULTS, Collections.EMPTY_SET, myLabel);
        verifyNoMoreInteractions(labelManager);
    }

    @Test
    public void contentIsCombinedAcrossMultipleSpaces() throws Exception
    {
        macroParameters.put("label", myLabel.getName());
        macroParameters.put("spaces", "XYZ, PZQ");

        Space xyz = mockSpace("XYZ");
        Space pzq = mockSpace("PZQ");
        when(labelManager.getSpacesContainingContentWithLabel(myLabel)).thenReturn(asList(xyz, pzq));

        ContentEntityObject xyz1 = mockPage("PageXyz1"),
                xyz2 = mockPage("PageXyz2"),
                pzq1 = mockPage("PagePzq1"),
                pzq2 = mockPage("PagePzq2");

        List<ContentEntityObject> restrictedContent = asList(xyz1, xyz2, pzq1, pzq2);

        Set<String> spaceKeys = Sets.newHashSet("XYZ", "PZQ");
        mockLabelManagerGetContentForMyLabel(spaceKeys, restrictedContent);
        when(permissionManager.getPermittedEntities(any(User.class), eq(Permission.VIEW), anyList())).thenReturn(restrictedContent);

        // Generics suX0r
        final Iterable<ContentEntityObject> mergedContent = contentRetriever.getContentWithMetaData(myLabel.getName(), Arrays.asList(
            "XYZ, @PZQ"), "");

        assertThat(mergedContent, containsInAnyOrder(xyz1, xyz2, pzq1, pzq2));
    }

    @Test
    public void contentIsSortedByReverseModificationDate() throws Exception
    {
        macroParameters.put("label", myLabel.getName());
        macroParameters.put("spaces", "XYZ");

        Space xyz = mockSpace("XYZ");
        when(labelManager.getSpacesContainingContentWithLabel(myLabel)).thenReturn(asList(xyz));

        ContentEntityObject xyz1 = mockPage("Zebra"),
                xyz2 = mockPage("Xylophone"),
                xyz3 = mockPage("Yoyo");

        //We don't really care what the date is, as long as they're in order
        when(xyz1.getLastModificationDate()).thenReturn(new Date(1));
        when(xyz2.getLastModificationDate()).thenReturn(new Date(10));
        when(xyz3.getLastModificationDate()).thenReturn(new Date(100));

        List<ContentEntityObject> xyzContent = asList(xyz1, xyz2, xyz3);
        Set<String> spaceKeys = Sets.newHashSet("XYZ");

        mockLabelManagerGetContentForMyLabel(spaceKeys, xyzContent);
        when(permissionManager.getPermittedEntities(any(User.class), eq(Permission.VIEW), eq(xyzContent))).thenReturn(xyzContent);

        final List<ContentEntityObject> orderedContent = contentRetriever.getContentWithMetaData(myLabel.getName(),
            Arrays.asList("XYZ"), "");

        List<ContentEntityObject> expectedList = asList(xyz3, xyz2, xyz1);
        assertEquals(expectedList, orderedContent);
    }

    @Test
    public void contentIsFilteredByViewPermissionsWhenFetchingByLabelAndCurrentSpace() throws Exception
    {
        macroParameters.put("label", myLabel.getName());

        Space xyz = mockSpace("XYZ");
        when(spaceManager.getSpace("XYZ")).thenReturn(xyz);
        when(labelManager.getSpacesContainingContentWithLabel(myLabel)).thenReturn(asList(xyz));

        ContentEntityObject xyz1 = mockPage("Zebra"),
                xyz2 = mockPage("Xylophone"),
                xyz3 = mockPage("Yoyo");

        List<ContentEntityObject> labeledPages = asList(xyz1, xyz2, xyz3);
        List<ContentEntityObject> unrestrictedPages = asList(xyz1);

        Set<String> spaceKeys = Sets.newHashSet("XYZ");
        mockLabelManagerGetContentForMyLabel(spaceKeys, labeledPages);
        when(permissionManager.getPermittedEntities(any(User.class), eq(Permission.VIEW), eq(labeledPages))).thenReturn(
            unrestrictedPages);

        final List<ContentEntityObject> unrestrictedContent = contentRetriever.getContentWithMetaData(myLabel.getName(), Arrays.asList("XYZ"), "");
        verify(permissionManager).getPermittedEntities(any(User.class), eq(Permission.VIEW), anyList());

        assertEquals(unrestrictedPages, unrestrictedContent);
    }

    @Test
    public void contentIsFilteredByViewPermissionsWhenFetchingByLabelAndSpace() throws Exception
    {
        macroParameters.put("label", myLabel.getName());
        macroParameters.put("spaces", "XYZ");

        Space xyz = mockSpace("XYZ");
        when(labelManager.getSpacesContainingContentWithLabel(myLabel)).thenReturn(asList(xyz));

        ContentEntityObject xyz1 = mockPage("Zebra"),
                xyz2 = mockPage("Xylophone"),
                xyz3 = mockPage("Yoyo");

        List<ContentEntityObject> labeledPages = asList(xyz1, xyz2, xyz3);
        List<ContentEntityObject> unrestrictedPages = asList(xyz1);
        Set<String> spaceKeys = Sets.newHashSet("XYZ");

        mockLabelManagerGetContentForMyLabel(spaceKeys, labeledPages);
        when(permissionManager.getPermittedEntities(any(User.class), eq(Permission.VIEW), eq(labeledPages))).thenReturn(
            unrestrictedPages);

        final List<ContentEntityObject> unrestrictedContent = contentRetriever.getContentWithMetaData(myLabel.getName(), Arrays.asList("XYZ"), "");
        verify(permissionManager).getPermittedEntities(any(User.class), eq(Permission.VIEW), anyList());

        assertEquals(unrestrictedPages, unrestrictedContent);
    }

    @Test
    public void contentIsFilteredByViewPermissionsWhenFetchingByLabelForAllSpaces() throws Exception
    {
        macroParameters.put("label", myLabel.getName());
        macroParameters.put("spaces", "@all, XYZ");

        ContentEntityObject xyz1 = mockPage("Zebra"),
                xyz2 = mockPage("Xylophone"),
                xyz3 = mockPage("Yoyo");

        List<ContentEntityObject> labeledPages = asList(xyz1, xyz2, xyz3);
        List<ContentEntityObject> unrestrictedPages = asList(xyz1);
        Set<String> spaceKeys = Sets.newHashSet();

        mockLabelManagerGetContentForMyLabel(spaceKeys, labeledPages);
        when(permissionManager.getPermittedEntities(any(User.class), eq(Permission.VIEW), eq(labeledPages))).thenReturn(
            unrestrictedPages);

        final List<ContentEntityObject> unrestrictedContent = contentRetriever.getContentWithMetaData(myLabel.getName(), Arrays.asList("XYZ", "@all"), "" );
        verify(permissionManager).getPermittedEntities(any(User.class), eq(Permission.VIEW), anyList());

        assertEquals(unrestrictedPages, unrestrictedContent);
    }

    private void mockLabelManagerGetContentForMyLabel(Set<String> spaceKeys, List<ContentEntityObject> labeledPages)
    {
        PartialList<ContentEntityObject> partialList = new PartialList<ContentEntityObject>(labeledPages.size(), NO_OFFSET, labeledPages);
        when(labelManager.getContentInSpacesForAllLabels(NO_OFFSET, NO_MAX_RESULTS, spaceKeys, myLabel)).thenReturn(partialList);
    }

    @Test
    public void errorShownIfLabelParameterNotSpecified() throws Exception
    {
        assertInternalMacroExceptionMessage(null, "detailsmacro.error.no.label.specified");
    }

    @Test
    public void errorShownIfLabelSpecifiedIsNotValidLabelName() throws Exception
    {
        assertInternalMacroExceptionMessage("^'invalid-label'^", "detailsmacro.error.label.name.not.valid");
    }

    @Test
    public void errorRenderedIfLabelSpecifiedIsInvalid() throws Exception
    {
        assertInternalMacroExceptionMessage("label1", "detailssummary.error.labeldoesnotexist");
    }

    private void assertInternalMacroExceptionMessage(String label, String errorMessageKey)
    {
        try
        {
            contentRetriever.getContentWithMetaData(label, Collections.EMPTY_LIST, "");
            fail("Exception should be thrown.");
        }
        catch (MacroExecutionException e)
        {
            assertThat(e.getMessage(), is(errorMessageKey));
        }
    }

    private static final Answer<String> RETURN_FIRST_ARGUMENT_MESSAGE = new Answer<String>()
    {
        @Override
        public String answer(InvocationOnMock invocationOnMock) throws Throwable
        {
            return invocationOnMock.getArguments()[0].toString();
        }
    };
}
