package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.pages.Page;
import com.atlassian.plugin.webresource.WebResourceManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class TestDetailsMacro
{
    @Mock
    private WebResourceManager webResourceManager;

    private DetailsMacro detailsMacro;

    @Before
    public void setUp() throws Exception
    {
        detailsMacro = new DetailsMacro(webResourceManager);
    }

    @Test
    public void detailsTableSurroundedByDiv() throws Exception
    {
        final String label = "test";
        
        assertEquals(
                "<div class='plugin-tabmeta-details'><table></table></div>",
                detailsMacro.execute(
                        new HashMap<String, String>()
                        {
                            {
                                put("label", label);
                            }
                        },
                        "<table></table>",
                        new Page().toPageContext()
                )
        );
    }

    @Test
    public void hideDetailsTable() throws Exception
    {
        assertEquals(
                "",
                detailsMacro.execute(
                        new HashMap<String, String>()
                        {
                            {
                                put("hidden", "true");
                            }
                        },
                        "<table></table>",
                        new Page().toPageContext()
                )
        );
    }
}
