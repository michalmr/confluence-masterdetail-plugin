package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.RenderedContentCleaner;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.extra.masterdetail.entities.PaginatedDetailLines;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.like.LikeManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.CommentManager;
import com.atlassian.confluence.plugins.createcontent.api.services.CreateButtonService;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.renderer.template.TemplateRenderer;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.user.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;

import static com.atlassian.confluence.content.render.xhtml.ConversionContextOutputDeviceType.DESKTOP;
import static com.atlassian.confluence.macro.Macro.BodyType.NONE;
import static com.atlassian.confluence.macro.Macro.OutputType.BLOCK;
import static com.google.common.collect.Maps.newHashMap;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDetailsSummaryMacro
{
    public static final String TEST_SPACE_KEY = "TST";
    public static final Space TEST_SPACE = new Space(TEST_SPACE_KEY);

    @Mock
    private I18NBeanFactory i18NBeanFactory;

    @Mock
    private LocaleManager localeManager;

    @Mock
    private XhtmlContent xhtmlContent;

    @Mock
    private LabelManager labelManager;

    @Mock
    private WebResourceManager webResourceManager;

    @Mock
    private SpaceManager spaceManager;

    @Mock
    private RenderedContentCleaner cleaner;
    
    @Mock
    private CommentManager commentManager;

    @Mock
    private LikeManager likeManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private CreateButtonService createButtonService;

    @Mock
    private TemplateRenderer templateRenderer;

    @Mock
    private SettingsManager settingsManager;

    @Mock
    private ContentRetriever contentRetriever;

    @Mock
    private DetailsSummaryBuilder detailsSummaryBuilder;

    private DetailsSummaryMacro macro;
    private final Map<String, String> macroParameters = newHashMap();
    private final Label myLabel = new Label("mylabel");

    @Before
    public void setup()
    {
        final Locale locale = Locale.getDefault();
        when(localeManager.getLocale(any(User.class))).thenReturn(locale);

        final I18NBean i18NBean = mock(I18NBean.class);
        when(i18NBeanFactory.getI18NBean(locale)).thenReturn(i18NBean);

        when(i18NBean.getText(anyString())).thenAnswer(RETURN_FIRST_ARGUMENT);
        when(i18NBean.getText(anyString(), any(String[].class))).thenAnswer(RETURN_FIRST_ARGUMENT);

        when(labelManager.getLabel(myLabel)).thenReturn(myLabel);
        when(spaceManager.getSpace(TEST_SPACE_KEY)).thenReturn(TEST_SPACE);

        macro = new DetailsSummaryMacro(
                localeManager, i18NBeanFactory, xhtmlContent,
                spaceManager, webResourceManager, createButtonService, contentRetriever, detailsSummaryBuilder,
            settingsManager,templateRenderer, permissionManager);
    }

    @Test
    public void errorsFromMacroExceptionsAreWrapped() throws Exception
    {
        ConversionContext conversionContext = conversionContext();
        String errorKey = "it.is.bad";
        when(contentRetriever.getContentWithMetaData(null, Collections.EMPTY_LIST, TEST_SPACE_KEY)).thenThrow(
            new MacroExecutionException(errorKey));

        String expectedErrorMarkup = expectedErrorMarkup(errorKey);
        assertEquals(expectedErrorMarkup, macro.execute(macroParameters, EMPTY, conversionContext));
    }

    @Test
    public void errorShownIfRenderedOnNonSpaceContentEntityObjects() throws Exception
    {
        assertEquals(expectedErrorMarkup("detailssummary.error.must.be.inside.space"),
                macro.execute(macroParameters, EMPTY, conversionContextForNonSpaceContentEntity()));
    }

    @Test
    public void macroGeneratesBlockHtml()
    {
        assertEquals(BLOCK, macro.getOutputType());
    }

    @Test
    public void macroHasNoBody()
    {
        assertEquals(NONE, macro.getBodyType());
    }

    @Test
    public void blankExperienceWithButton() throws Exception
    {
        String contentBlueprintId = "12345";
        String blueprintModuleCompleteKey = "com.atlassian.confluence:foo";
        String buttonLabelKey = "blank.button.label";

        macroParameters.put("blueprintModuleCompleteKey", blueprintModuleCompleteKey);
        macroParameters.put("contentBlueprintId", contentBlueprintId);
        macroParameters.put("createButtonLabel", buttonLabelKey);

        PaginatedDetailLines paginatedDetails = mock(PaginatedDetailLines.class);
        when(paginatedDetails.getDetailLines()).thenReturn(Collections.EMPTY_LIST);
        when(detailsSummaryBuilder.getPaginatedDetailLines(anyInt(), anyInt(), anyBoolean(), anyBoolean(), anyString(), anyString(), anyBoolean(), anyList(), anyString()))
            .thenReturn(paginatedDetails);

        macro.execute(macroParameters, EMPTY, conversionContext());

        verify(createButtonService).renderBlueprintButton(TEST_SPACE, contentBlueprintId, blueprintModuleCompleteKey, buttonLabelKey, null);
    }

    private String expectedErrorMarkup(String message)
    {
        return "<div class=\"error\"><span class=\"error\">" + message + "</span> </div>";
    }

    private static final Answer<String> RETURN_FIRST_ARGUMENT = new Answer<String>()
    {
        public String answer(InvocationOnMock invocationOnMock) throws Throwable
        {
            return invocationOnMock.getArguments()[0].toString();
        }
    };

    private static ConversionContext conversionContextForNonSpaceContentEntity()
    {
        return new DefaultConversionContext(new Comment().toPageContext());
    }

    private static ConversionContext conversionContext()
    {
        SpaceContentEntityObject page = mock(SpaceContentEntityObject.class);
        PageContext pageContext = mock(PageContext.class);
        when(pageContext.getEntity()).thenReturn(page);
        when(pageContext.getSpaceKey()).thenReturn(TEST_SPACE_KEY);
        when(pageContext.getOutputDeviceType()).thenReturn(DESKTOP);

        return new DefaultConversionContext(pageContext);
    }
}
