package com.atlassian.confluence.extra.masterdetail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestExtractedDetailsComparator
{
    @Test
    public void sortTitle()
    {
        List<ExtractedDetails> details = setupTitles("W", "a", "", "B", "ab");

        ExtractedDetailsComparator titleComparator = new ExtractedDetailsComparator("title", false);
        Collections.sort(details, titleComparator);

        String[] expected = new String []{"", "a", "ab", "B", "W"};

        expectTitles(details, expected);
    }

    @Test
    public void reverseSortTitle()
    {
        List<ExtractedDetails> details = setupTitles("W", "a", "", "B", "ab");

        ExtractedDetailsComparator reversedComparator = new ExtractedDetailsComparator("title", true);
        Collections.sort(details, reversedComparator);

        String[] expected = new String []{"W", "B", "ab", "a", ""};
        expectTitles(details, expected);
    }

    @Test
    public void nonTitleKey()
    {
        String key = "other";
        List<ExtractedDetails> details = setupValues(key, "zero", "one", "two", "three", "four", "five");

        ExtractedDetailsComparator comparator = new ExtractedDetailsComparator(key, false);
        Collections.sort(details, comparator);

        String[] expected = new String []{"5", "4", "1", "3", "2", "0"};
        expectTitles(details, expected);
    }

    private void expectTitles(List<ExtractedDetails> details, String[] expected)
    {
        for (int i=0; i<expected.length; i++)
        {
            assertEquals(expected[i], details.get(i).getTitle());
        }
    }

    private List<ExtractedDetails> setupValues(String key, String... values)
    {
        List<ExtractedDetails> details = newArrayList();
        for(int i=0; i<values.length; i++)
        {
            ExtractedDetails detail = mock(ExtractedDetails.class);
            when(detail.getTitle()).thenReturn("" + i);
            when(detail.getDetailStorageFormat(key)).thenReturn(values[i]);
            details.add(detail);
        }
        return details;
    }

    private List<ExtractedDetails> setupTitles(String... titles)
    {
        List<ExtractedDetails> details = newArrayList();
        for (String title : titles)
        {
            ExtractedDetails detail = mock(ExtractedDetails.class);
            when(detail.getTitle()).thenReturn(title);
            details.add(detail);
        }
        return details;
    }
}
