package com.atlassian.confluence.extra.masterdetail.utils;

import com.atlassian.confluence.it.Space;
import org.apache.commons.lang.StringUtils;

import java.util.Iterator;
import java.util.Map;

import static com.google.common.collect.Maps.newLinkedHashMap;
import static java.util.Arrays.asList;
import static org.apache.commons.lang.StringUtils.EMPTY;

/**
 * Creates markup for pages used in Acceptance tests.
 */
public class MarkupHelper
{
    public static final String ALL_SPACES = "@all";

    /**
     * Markup for a details macro with a simple "property1", "value1" pair.
     */
    public static String makeDefaultDetailsMarkup()
    {
        return makeDetailsMarkup("property1", "value1");
    }

    public static String makeDetailsMarkup(String... properties)
    {
        String rows = serializeMetadata(buildMetadataMap(properties));

        return "<ac:macro ac:name=\"details\"><ac:rich-text-body>\n" +
                "   <table class=\"confluenceTable\">\n" +
                "       <tbody>\n" + rows + "</tbody>" +
                "   </table>" +
                "</ac:rich-text-body></ac:macro>";
    }

    public static String makeDetailsMarkup(final String id, final Boolean hidden, final String... properties)
    {
        String rows = serializeMetadata(buildMetadataMap(properties));

        StringBuilder macro = new StringBuilder("<ac:macro ac:name=\"details\">");

        if (id != null)
            macro.append("<ac:parameter ac:name=\"id\">").append(id).append("</ac:parameter>");

        if (hidden != null && hidden)
            macro.append("<ac:parameter ac:name=\"hidden\">true</ac:parameter>");

            macro.append("<ac:rich-text-body>\n")
                .append("<table class=\"confluenceTable\">\n")
                .append("<tbody>\n").append(rows).append("</tbody>")
                .append("</table>")
                .append("</ac:rich-text-body></ac:macro>");

        return macro.toString();
    }

    public static Map<String, String> buildMetadataMap(String... detailOrValues)
    {
        Map<String, String> detailMap = newLinkedHashMap();
        for (Iterator<String> detailOrValue = asList(detailOrValues).iterator(); detailOrValue.hasNext();)
        {
            String property = detailOrValue.next();
            String value = detailOrValue.hasNext() ? detailOrValue.next() : EMPTY;

            detailMap.put(property, value);
        }

        return detailMap;
    }

    public static String serializeMetadata(Map<String, String> properties)
    {
        StringBuilder serializedBuffer = new StringBuilder();

        for (Map.Entry keyValuePair : properties.entrySet())
        {
            String row = "<tr>" +
                    "    <td class=\"confluenceTd\">" + keyValuePair.getKey() + "</td>" +
                    "    <td class=\"confluenceTd\">" + keyValuePair.getValue() + "</td>" +
                    "</tr>";
            serializedBuffer.append(row);
        }

        return serializedBuffer.toString();
    }

    public static String makeSummaryMarkup(String label, String spaceKeys)
    {
        return makeSummaryMarkup(label, false, false, spaceKeys);
    }

    public static String makeSummaryMarkup(String label, boolean showCommentsCount, boolean showLikesCount, String spaceKeys)
    {
        StringBuilder macro = new StringBuilder("<ac:macro ac:name=\"detailssummary\">")
                .append("<ac:parameter ac:name=\"label\">").append(label).append("</ac:parameter>");
        if (StringUtils.isNotBlank(spaceKeys))
        {
            macro.append("<ac:parameter ac:name=\"spaces\">").append(spaceKeys).append("</ac:parameter>");
        }
        if (showCommentsCount)
        {
            macro.append("<ac:parameter ac:name=\"showCommentsCount\">true</ac:parameter>");
        }
        if (showLikesCount)
        {
            macro.append("<ac:parameter ac:name=\"showLikesCount\">true</ac:parameter>");
        }
        macro.append("</ac:macro>");
        return macro.toString();
    }

    public static String makeSummaryMarkup(String label, Space space)
    {
        return makeSummaryMarkup(label, null, space.getKey(), null, null, null, null);
    }

    public static String makeSummaryMarkup(String label, Space space, String pageSize, String sortBy)
    {
        return makeSummaryMarkup(label, null, space.getKey(), null, null, pageSize, sortBy);
    }

    public static String makeSummaryMarkup(String label, String id, String spaces, String firstColumn, String headings, String pageSize, String sortBy)
    {
        StringBuilder markup = new StringBuilder();

        markup.append("<ac:macro ac:name=\"detailssummary\">")
            .append("<ac:parameter ac:name=\"label\">").append(label).append("</ac:parameter>");

        if (StringUtils.isNotBlank(firstColumn))
            markup.append("<ac:parameter ac:name=\"firstcolumn\">").append(firstColumn).append("</ac:parameter>");

        if (StringUtils.isNotBlank(spaces))
            markup.append("<ac:parameter ac:name=\"spaces\">").append(spaces).append("</ac:parameter>");

        if (StringUtils.isNotBlank(id))
            markup.append("<ac:parameter ac:name=\"id\">").append(id).append("</ac:parameter>");

        if (StringUtils.isNotBlank(pageSize))
            markup.append("<ac:parameter ac:name=\"pageSize\">").append(pageSize).append("</ac:parameter>");

        if (StringUtils.isNotBlank(sortBy))
            markup.append("<ac:parameter ac:name=\"sortBy\">").append(sortBy).append("</ac:parameter>");

        if (StringUtils.isNotBlank(headings))
            markup.append("<ac:parameter ac:name=\"headings\">").append(headings).append("</ac:parameter>");

        markup.append("</ac:macro>");
        return markup.toString();
    }

    public static String makeSummaryMarkupOfBlankExperienceMacro(String label, String blankTitle, String blankDescription,
                                                                 String blueprintModuleCompleteKey,
                                                                 String createButtonLabel)
    {
        StringBuilder markup = new StringBuilder();

        markup.append("<ac:macro ac:name=\"detailssummary\">" +
                "<ac:parameter ac:name=\"label\">" + label + "</ac:parameter>");
        markup.append(makeDetailsMarkup());
        markup.append("<ac:parameter ac:name=\"blueprintModuleCompleteKey\">" + blueprintModuleCompleteKey + "</ac:parameter>");
        markup.append("<ac:parameter ac:name=\"blankTitle\">" + blankTitle + "</ac:parameter>");
        markup.append("<ac:parameter ac:name=\"blankDescription\">" + blankDescription + "</ac:parameter>");
        markup.append("<ac:parameter ac:name=\"createButtonLabel\">" + createButtonLabel + "</ac:parameter>");
        markup.append("</ac:macro>");

        return markup.toString();
    }
}
