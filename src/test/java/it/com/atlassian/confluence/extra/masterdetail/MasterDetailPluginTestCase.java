package it.com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.extra.masterdetail.utils.MarkupHelper;
import com.atlassian.confluence.it.Attachment;
import com.atlassian.confluence.it.Comment;
import com.atlassian.confluence.it.ContentPermissionType;
import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.SpacePermission;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.content.security.ContentPermission;
import com.atlassian.confluence.it.plugin.TestPlugins;
import com.atlassian.confluence.it.rpc.ConfluenceRpc;
import com.atlassian.confluence.it.setup.PostSetupHelper;
import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import org.apache.commons.lang.StringUtils;

import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.isNotBlank;

public class MasterDetailPluginTestCase extends AbstractConfluencePluginWebTestCase
{
    private static final Space TEST_SPACE = new Space("tst", "Test Space", "Test Space Description");
    private static final Space TEST_SPACE2 = new Space("tst2", "Test Space 2", "Test Space 2 Description");
    private static final String TABLE_XPATH_ROOT = "//table[contains(@class,'metadata-summary-macro')]";
    private static final String DEFAULT_FIRST_HEADING = "Title";
    
    private ConfluenceRpc rpc;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        rpc = ConfluenceRpc.newInstance(getConfluenceWebTester().getBaseUrl());
        rpc.logIn(User.ADMIN);

        TestPlugins.installFuncTestRpcPlugin(rpc.getPluginHelper());

        rpc.removeAllSpaces();
        rpc.removeUser(User.TEST);
        rpc.createSpace(TEST_SPACE);
        rpc.createSpace(TEST_SPACE2);
    }

    public void testRenderDetails()
    {
        String property1 = "property1";
        String value1 = "value1";
        String property2 = "property2";
        String value2 = "value2";
        String content = MarkupHelper.makeDetailsMarkup(property1, value1, property2, value2);
        long testPageId = createPage(TEST_SPACE, getName(), content);

        viewPageById(testPageId);
        assertDetailsRow(0, property1, value1);
        assertDetailsRow(1, property2, value2);
    }

    public void testSummaryRendersAllMetadataOfContentWithSameLabelWithHeadingsOrderedAlphabetically()
    {
        String labelName = "label1";
        String property1 = "Property1";
        String value1 = "value1";
        String property2 = "Property2";
        String value2 = "value2";
        String property5 = "Property5";
        String value5 = "value5";
        String pageWithLabel1 = "pageWithLabel";

        String content1 = MarkupHelper.makeDetailsMarkup(property1, value1, property2, value2, property5, value5);
        long pageWithLabelId1 = createPage(TEST_SPACE, pageWithLabel1, content1, labelName);
        viewPageById(pageWithLabelId1);

        String property3 = "Property3";
        String value3 = "value3";
        String property4 = "Property4";
        String value4 = "value4";
        String property6 = "Property6";
        String value6 = "value6";
        String pageWithLabel2 = "pageWithLabel2";

        String content2 = MarkupHelper.makeDetailsMarkup(property3, value3, property4, value4, property6, value6);
        long pageWithLabelId2 = createPage(TEST_SPACE, pageWithLabel2, content2, labelName);
        viewPageById(pageWithLabelId2);

        long testPageId = createPage(TEST_SPACE, getName(), makeSummaryMarkup(labelName));

        viewPageById(testPageId);

        assertSummaryHeading(DEFAULT_FIRST_HEADING, property1, property2, property3, property4, property5, property6);
        assertSummaryRow(1, pageWithLabel2, EMPTY, EMPTY, value3, value4, EMPTY, value6);
        assertSummaryRow(2, pageWithLabel1, value1, value2, EMPTY, EMPTY, value5, EMPTY);
    }

    public void testSummaryWithSortbyParameter() //CONF-32256
    {
        String labelName = "label1";
        String propertyName = "Property 1";

        String content1 = MarkupHelper.makeDetailsMarkup(propertyName, "foo");
        String content2 = MarkupHelper.makeDetailsMarkup(propertyName, "bar");
        String content3 = MarkupHelper.makeDetailsMarkup(propertyName, "zzz");
        String title1 = "Page 1";
        String title2 = "Page 2";
        String title3 = "Page 3";
        createPage(TEST_SPACE, title1, content1, labelName);
        createPage(TEST_SPACE, title2, content2, labelName);
        createPage(TEST_SPACE, title3, content3, labelName);

        long testPageId = createPage(TEST_SPACE, getName(), MarkupHelper.makeSummaryMarkup(labelName, null, null,
            "Title", null, null, propertyName));

        viewPageById(testPageId);

        assertSummaryHeading(DEFAULT_FIRST_HEADING, propertyName);
        assertSummaryRow(1, title2);
        assertSummaryRow(2, title1);
        assertSummaryRow(3, title3);
    }

    public void testSummaryRendersAllMetadataOfContentWithSameLabelWithHeadingsInOrderSpecifiedByUser()
    {
        String labelName = "label1";
        String property1 = "property1";
        String value1 = "value1";
        String property2 = "property2";
        String value2 = "value2";
        String property5 = "property5";
        String value5 = "value5";
        String pageWithLabel1 = "pageWithLabel";

        String content1 = MarkupHelper.makeDetailsMarkup(property1, value1, property2, value2, property5, value5);
        long pageWithLabelId1 = createPage(TEST_SPACE, pageWithLabel1, content1, labelName);
        viewPageById(pageWithLabelId1);

        String property3 = "property3";
        String value3 = "value3";
        String property4 = "property4";
        String value4 = "value4";
        String property6 = "property6";
        String value6 = "value6";
        String pageWithLabel2 = "pageWithLabel2";

        String content2 = MarkupHelper.makeDetailsMarkup(property3, value3, property4, value4, property6, value6);
        long pageWithLabelId2 = createPage(TEST_SPACE, pageWithLabel2, content2, labelName);
        viewPageById(pageWithLabelId2);

        String headings = "property6,property4,property2,property5,property3,property1";
        String content = "<ac:macro ac:name=\"detailssummary\">" +
            "   <ac:parameter ac:name=\"label\">" + labelName + "</ac:parameter>" +
            "   <ac:parameter ac:name=\"headings\">" + headings + "</ac:parameter>" +
            "</ac:macro>";
        long testPageId = createPage(TEST_SPACE, getName(), content);
        viewPageById(testPageId);

        assertSummaryHeading(DEFAULT_FIRST_HEADING, property6, property4, property2, property5, property3, property1);
        assertSummaryRow(1, pageWithLabel2, value6, value4, EMPTY, EMPTY, value3, EMPTY);
        assertSummaryRow(2, pageWithLabel1, EMPTY, EMPTY, value2, value5, EMPTY, value1);
    }

    public void testOverlappingPropertiesAreRepeatedForEachContentThatHasThem()
    {
        String labelName = "label1";
        String property1 = "property1";
        String value1 = "value1";
        String property2 = "property2";
        String value2 = "value2";
        String property5 = "property5";
        String value5 = "value5";
        String pageWithLabel1 = "pageWithLabel";

        String content1 = MarkupHelper.makeDetailsMarkup(property1, value1, property2, value2, property5, value5);
        long pageWithLabelId1 = createPage(TEST_SPACE, pageWithLabel1, content1, labelName);
        viewPageById(pageWithLabelId1);

        String property3 = "property3";
        String value3 = "value3";
        String property4 = "property4";
        String value4 = "value4";
        String property6 = "property6";
        String value6 = "value6";
        String pageWithLabel2 = "pageWithLabel2";

        String content2 = MarkupHelper.makeDetailsMarkup(property1, value1, property3, value3, property2, value2,
            property4, value4, property6, value6);
        long pageWithLabelId2 = createPage(TEST_SPACE, pageWithLabel2, content2, labelName);
        viewPageById(pageWithLabelId2);

        long testPageId = createPage(TEST_SPACE, getName(), makeSummaryMarkup(labelName));

        viewPageById(testPageId);

        assertSummaryHeading(DEFAULT_FIRST_HEADING, property1, property2, property3, property4, property5,  property6);
        assertSummaryRow(1, pageWithLabel2, value1, value2, value3, value4, EMPTY, value6);
        assertSummaryRow(2, pageWithLabel1, value1, value2, EMPTY, EMPTY, value5, EMPTY);
    }

    public void testOnlyPageWithMetadataGetsRendered()
    {
        createPage(TEST_SPACE, "pageWithMetadata", MarkupHelper.makeDetailsMarkup("property1", "value1"), "label1");
        createPage(TEST_SPACE, "pageWithoutMetadata", "<ac:macro ac:name=\"cheese\" />", "label1");

        long testPageId = createPage(TEST_SPACE, getName(), makeSummaryMarkup("label1"));
        viewPageById(testPageId);
        assertLinkNotPresentWithText("pageWithoutMetadata");
    }

    // CONFDEV-16985
    public void testOnlyPagesWhichUserCanViewGetRendered()
    {
        long restrictedPageId = createPage(TEST_SPACE, "Page Restricted With Metadata", MarkupHelper.makeDetailsMarkup(
            "property1", "value1"), "label1");
        rpc.setPageRestriction(new ContentPermission(User.ADMIN, ContentPermissionType.VIEW), rpc.getExistingPage(restrictedPageId));

        createPage(TEST_SPACE, "Page With Metadata", MarkupHelper.makeDetailsMarkup("property1", "value1"), "label1");

        long pageWithMacro = createPage(TEST_SPACE, getName(), makeSummaryMarkup("label1"));

        User user = new User("badguy", "foo", "I am bad and I know it", "bad@evil.com");
        rpc.createUser(user);
        rpc.grantPermission(SpacePermission.VIEW, TEST_SPACE, user);

        getConfluenceWebTester().logout();
        getConfluenceWebTester().login("badguy", "foo");

        viewPageById(pageWithMacro);
        assertLinkPresentWithText("Page With Metadata");
        assertLinkNotPresentWithText("Page Restricted With Metadata");
        rpc.removeUser(user);
    }

    public void testPagesFromMultipleSpacesAreRendered()
    {
        createPage(TEST_SPACE, "page1", MarkupHelper.makeDetailsMarkup("property1", "value1"), "label1");
        createPage(TEST_SPACE2, "page2", MarkupHelper.makeDetailsMarkup("property1", "value2"), "label1");

        long testPageId = createPage(TEST_SPACE, getName(), makeSummaryMarkup("label1", TEST_SPACE.getKey(), TEST_SPACE2
                .getKey()));
        viewPageById(testPageId);
        assertSummaryRow(1, "page2", "value2");
        assertSummaryRow(2, "page1", "value1");
    }
    
    //CONF-31680
    public void testFormattingInProperties()
    {
        String property1 = "property1";
        String value1 = "value1";
        String property2 = "property2";
        String value2 = "value2";
        String pageTitle = "pageWithMetadata";
        createPage(TEST_SPACE, pageTitle, MarkupHelper.makeDetailsMarkup("<strong>" + property1 + "</strong>", value1,
            property2, "<strong>" + value2 + "</strong>"), "label1");

        long testPageId = createPage(TEST_SPACE, getName(), makeSummaryMarkup("label1"));
        viewPageById(testPageId);
        assertSummaryHeading(DEFAULT_FIRST_HEADING, property1, property2);
        assertSummaryRow(1, pageTitle, value1, value2);
        assertSummary(1, 3, "/strong", value2);
    }

    //CONF-31363
    public void testNonEnglishProperties()
    {
        String label = "foo";
        String koreanProperty = "\uC778\uC0AC";
        String koreanValue = "\uC548\uB155\uD558\uC138\uC694";
        String chineseProperty = "\u95EE\u5019";
        String chineseValue = "\u4F60\u597D";
        Page page = new Page(TEST_SPACE, "Page with some asian characters", MarkupHelper.makeDetailsMarkup(
            koreanProperty, koreanValue, chineseProperty, chineseValue));
        Page reportPage = new Page(TEST_SPACE, "Report page", makeSummaryMarkup(label));
        rpc.createPage(page);
        rpc.createPage(reportPage);
        rpc.addLabel(label, page);

        viewPageById(reportPage.getId());
        assertSummaryHeading(DEFAULT_FIRST_HEADING, chineseProperty, koreanProperty);
        assertSummaryRow(1, page.getTitle(), chineseValue, koreanValue);
    }

    public void testNonEnglishHeadingParameter()
    {
        String label = "foo";
        String koreanProperty = "\uC778\uC0AC";
        String koreanValue = "\uC548\uB155\uD558\uC138\uC694";
        String chineseProperty = "\u95EE\u5019";
        String chineseValue = "\u4F60\u597D";
        Page page = new Page(TEST_SPACE, "Page with some asian characters", MarkupHelper.makeDetailsMarkup(
            koreanProperty, koreanValue, chineseProperty, chineseValue));
        Page reportPage = new Page(TEST_SPACE, "Report page", MarkupHelper.makeSummaryMarkup(label, null, null,
            "Page title", StringUtils.join(new String[]{koreanProperty}), null, null));
        rpc.createPage(page);
        rpc.createPage(reportPage);
        rpc.addLabel(label, page);

        viewPageById(reportPage.getId());
        assertSummaryHeading("Page title", koreanProperty);
        assertSummaryRow(1, page.getTitle(), koreanValue);
        assertTextNotPresent(chineseProperty);
        assertTextNotPresent(chineseValue);
    }

    public void testNonEnglishSortParameter()
    {
        String label = "foo";
        String umlautA = "\u00E4";
        Page page1 = new Page(TEST_SPACE, "Page1", MarkupHelper.makeDetailsMarkup(umlautA, "1"));
        Page page2 = new Page(TEST_SPACE, "Page2", MarkupHelper.makeDetailsMarkup(umlautA, "2"));
        Page page3 = new Page(TEST_SPACE, "Page3", MarkupHelper.makeDetailsMarkup(umlautA, "3"));

        Page reportPage = new Page(TEST_SPACE, "Report page", MarkupHelper.makeSummaryMarkup(label, null, null,
            "Page title", umlautA, null, umlautA));
        //default sort order is last modified, so creating 1,2,3 would normally be listed as 3,2,1
        rpc.createPage(page1);
        rpc.createPage(page2);
        rpc.createPage(page3);
        rpc.createPage(reportPage);
        rpc.addLabel(label, page1);
        rpc.addLabel(label, page2);
        rpc.addLabel(label, page3);

        viewPageById(reportPage.getId());
        assertSummaryHeading("Page title", umlautA);
        assertSummaryRow(1, page1.getTitle(), "1");
        assertSummaryRow(2, page2.getTitle(), "2");
        assertSummaryRow(3, page3.getTitle(), "3");
    }

    public void testNonEnglishIdParameter()
    {
        String label = "foo";
        String umlautA = "\u00E4";
        String id = umlautA;
        Page page1 = new Page(TEST_SPACE, "Page1", MarkupHelper.makeDetailsMarkup(id, Boolean.FALSE, umlautA, "1"));
        Page page2 = new Page(TEST_SPACE2, "Page2", MarkupHelper.makeDetailsMarkup(id, Boolean.FALSE, umlautA, "2"));
        Page page3 = new Page(TEST_SPACE, "Page3", MarkupHelper.makeDetailsMarkup(umlautA, "3"));

        Page reportPage = new Page(TEST_SPACE, "Report page", MarkupHelper.makeSummaryMarkup(label, id,
            TEST_SPACE.getKey() + "," + TEST_SPACE2.getKey(), "Page title", null, null, "title"));

        rpc.createPage(page1);
        rpc.createPage(page2);
        rpc.createPage(page3);
        rpc.createPage(reportPage);
        rpc.addLabel(label, page1);
        rpc.addLabel(label, page2);
        rpc.addLabel(label, page3);

        viewPageById(reportPage.getId());
        assertSummaryRow(1, page1.getTitle(), "1");
        assertSummaryRow(2, page2.getTitle(), "2");
        assertLinkNotPresentWithExactText(page3.getTitle());
    }

    public void testCommentsCountIsShown()
    {
        createReportedPageWithSingleCommentSingleLike();
        long testPageId = createPage(TEST_SPACE, getName(), makeSummaryMarkup("label1", true, false, TEST_SPACE.getKey()));
        viewPageById(testPageId);

        assertCountPresence("comment", true);
        assertCountPresence("like", false);
    }

    public void testLikesCountIsShown()
    {
        createReportedPageWithSingleCommentSingleLike();
        long testPageId = createPage(TEST_SPACE, getName(), makeSummaryMarkup("label1", false, true, TEST_SPACE.getKey()));
        viewPageById(testPageId);

        assertCountPresence("comment", false);
        assertCountPresence("like", true);
    }

    public void testBothCommentsAndLikesCountAreShown()
    {
        createReportedPageWithSingleCommentSingleLike();
        long testPageId = createPage(TEST_SPACE, getName(), makeSummaryMarkup("label1", true, true, TEST_SPACE.getKey()));
        viewPageById(testPageId);

        assertCountPresence("comment", true);
        assertCountPresence("like", true);
    }

    public void testDefaultNoSocialColumn()
    {
        createReportedPageWithSingleCommentSingleLike();
        long testPageId = createPage(TEST_SPACE, getName(), makeSummaryMarkup("label1", false, false, TEST_SPACE.getKey()));
        viewPageById(testPageId);

        assertElementNotPresentByXPath("//table/tbody/tr/td[@class='social']");
    }

    // TABMETA-30
    public void testSummaryRendersMacrosInDetails()
    {
        String label = "label1";
        String property1 = "property1";
        String value1 = "<ac:macro ac:name=\"cheese\" />";
        String title = "pageWithLabel";

        String content = MarkupHelper.makeDetailsMarkup(property1, value1);

        createPage(TEST_SPACE, title, content, label);

        long summaryPage = createPage(TEST_SPACE, getName(), makeSummaryMarkup(label));
        viewPageById(summaryPage);

        String renderedMacro = "I like cheese!";
        assertSummaryRow(1, title, renderedMacro);
    }

    //CONF-30628
    public void testSummaryRendersImageInDetails()
    {
        String title = "pageWithLabel";
        String label = "label1";
        String fileName = "confluence-is-cool.png";
        String property1 = "image";
        String value1 = "<ac:image><ri:attachment ri:filename=\"" + fileName + "\" /></ac:image>";

        String content = MarkupHelper.makeDetailsMarkup(property1, value1);

        Page page = new Page(TEST_SPACE, title, content);
        Attachment attachment = new Attachment(page, fileName, "image/png", fileName);


        rpc.createPage(page);
        rpc.createAttachment(attachment);
        rpc.addLabel(label, page);

        long summaryPage = createPage(TEST_SPACE, getName(), makeSummaryMarkup(label));
        viewPageById(summaryPage);

        assertElementPresentByXPath("//img[contains(@src, '" + fileName + "')]");
    }

    //CONFDEV-17046
    public void testCustomFirstColumnHeading()
    {
        createPage(TEST_SPACE, "page", MarkupHelper.makeDetailsMarkup("Property1", "value1"), "label");

        String firstColumnHeading = "Page Titles";
        long testPageId = createPage(TEST_SPACE, getName(), MarkupHelper.makeSummaryMarkup("label", null, null,
            firstColumnHeading, "Property1", null, null));
        viewPageById(testPageId);
        assertSummaryHeading(firstColumnHeading, "Property1");
        assertSummaryRow(1, "page", "value1");
    }

    public void testLongFirstColumnHeadingIsTruncated()
    {
        createPage(TEST_SPACE, "page", MarkupHelper.makeDetailsMarkup("Property1", "value1"), "label");

        String firstColumnHeading = "Wheeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee";
        long testPageId = createPage(TEST_SPACE, getName(), MarkupHelper.makeSummaryMarkup("label", null, null,
            firstColumnHeading, "Property1", null, null));
        viewPageById(testPageId);

        assertSummaryHeading("Wheeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee...", "Property1");
        assertSummaryRow(1, "page", "value1");
    }

    public void testSummaryWithEmptyPagePropertyKey()
    {
        createPage(TEST_SPACE, "page", MarkupHelper.makeDetailsMarkup("&nbsp;", "value1"), "label");

        String firstColumnHeading = "first col";
        long testPageId = createPage(TEST_SPACE, getName(), MarkupHelper.makeSummaryMarkup("label", null, null,
            firstColumnHeading, null, null, null));
        viewPageById(testPageId);

        assertTextNotPresent("&nbsp;");
    }

    public void testSummaryWithNoContent()
    {
        String firstColumnHeading = "Page Titles";

        //create a page with the label - so the label is valid
        Page page = new Page(TEST_SPACE, "Some page", "");
        rpc.createPage(page);
        rpc.addLabel("label", page);

        long testPageId = createPage(TEST_SPACE, getName(), MarkupHelper.makeSummaryMarkup("label", null, null,
            firstColumnHeading, StringUtils.join(new String[]{}), null, null));
        viewPageById(testPageId);
        assertSummaryHeading(firstColumnHeading);
        assertTextPresent("No content found");
    }

    public void testSummaryReportsPropertyIgnoringNbspAndSpans()
    {
        String property1 = "property1";
        String value1 = "value1";
        String value2 = "value2";
        String value3 = "value3";
        String label = "mylabel";
        String nbsp = "&nbsp;";
        String title1 = "properties one";
        String title2 = "properties two";
        String title3 = "properties three";

        String content = MarkupHelper.makeDetailsMarkup(property1 + nbsp, value1);
        createPage(TEST_SPACE, title1, content, label);

        String content2 = MarkupHelper.makeDetailsMarkup("<span>" + property1 + "</span>", value2);
        createPage(TEST_SPACE, title2, content2, label);

        String content3 = MarkupHelper.makeDetailsMarkup(property1, value3);
        createPage(TEST_SPACE, title3, content3, label);

        long reportPageId = createPage(TEST_SPACE, "Report page", MarkupHelper.makeSummaryMarkup(label, null, TEST_SPACE.getKey(), null, property1, null, property1));
        viewPageById(reportPageId);

        assertSummaryHeading(DEFAULT_FIRST_HEADING, property1);
        assertSummaryRow(1, title1, value1);
        assertSummaryRow(2, title2, value2);
        assertSummaryRow(3, title3, value3);
    }

    public void testBlankExperienceMacro()
    {
        String label = "label";
        String blankTitle = "blank title";
        String blankDescription = "this is description of blank page";
        String blueprintModuleCompleteKey = "com.atlassian.confluence.plugins.confluence-software-blueprints:requirements-blueprint";
        String createButtonLabel = "create button label";

        rpc.logIn(User.ADMIN);
        rpc.addLabelToSpace(label, TEST_SPACE);

        long testPageId = createPage(TEST_SPACE, getName(),
            MarkupHelper.makeSummaryMarkupOfBlankExperienceMacro(
                label, blankTitle, blankDescription, blueprintModuleCompleteKey, createButtonLabel));
        viewPageById(testPageId);

        assertElementPresentByXPath("//div[@class='blueprint-blank-experience']");
        assertTextPresent(blankDescription);
        assertLinkPresentWithExactText(createButtonLabel);
    }

//    public void testBlankExperienceMacroWithNoCreatePermission()
//    {
//        String label = "label";
//        String blankTitle = "blank title";
//        String blankDescription = "this is description of blank page";
//        String blueprintModuleCompleteKey = "com.atlassian.confluence.plugins.confluence-software-blueprints:requirements-blueprint";
//        String createButtonLabel = "create button label";
//
//        rpc.logIn(User.ADMIN);
//        rpc.addLabelToSpace(label, TEST_SPACE);
//
//        long testPageId = createPage(TEST_SPACE, getName(),
//            MarkupHelper.makeSummaryMarkupOfBlankExperienceMacro(
//                label, blankTitle, blankDescription, blueprintModuleCompleteKey, createButtonLabel));
//
//        rpc.createUser(User.TEST);
//        rpc.grantPermission(SpacePermission.VIEW, TEST_SPACE, User.TEST);
//
//        logout();
//        login(User.TEST.getUsername(), User.TEST.getPassword());
//        viewPageById(testPageId);
//
//        assertElementPresentByXPath("//div[@class='blueprint-blank-experience']");
//        assertTextPresent(blankDescription);
//        assertLinkNotPresentWithText(createButtonLabel);
//    }

    /**
     * 
     * @param type "like" or "comment"
     * @param nthPosition the row to perform assertion
     * @param isPresent If true, assert the count is 1. If false, assert the count isn't present
     */
    private void assertCountPresence(String type, boolean isPresent)
    {
        if (!"like".equals(type) && !"comment".equals(type))
        {
            throw new IllegalArgumentException("Only like or comment is accepted as type parameter");
        }

        final String xpath = "//table/tbody/tr/td[@class='social']/span[@class='icon icon-" + type + "']/following-sibling::span[1]";
        if (isPresent)
        {
            String countText = getElementTextByXPath(xpath);
            assertEquals(countText, "1");
        }
        else
        {
            assertElementNotPresentByXPath(xpath);
        }
    }

    private void createReportedPageWithSingleCommentSingleLike()
    {
        long page1Id = createPage(TEST_SPACE, "page1", MarkupHelper.makeDetailsMarkup("property1", "value1"), "label1");
        Page page1 = rpc.getExistingPage(page1Id);
        rpc.likes.like(page1, User.ADMIN);
        Comment comment = new Comment(page1, "Page 1 comment");
        rpc.createComment(comment);
    }

    private static String makeSummaryMarkup(String label, String... spaceKeys)
    {
        return makeSummaryMarkup(label, false, false, spaceKeys);
    }

    private static String makeSummaryMarkup(String label, boolean showCommentsCount, boolean showLikesCount, String... spaceKeys)
    {
        return MarkupHelper.makeSummaryMarkup(label, showCommentsCount, showLikesCount, StringUtils.join(spaceKeys,
            ","));
    }

    private long createPage(Space space, String title, String content)
    {
        return createPage(space, title, content, null);
    }

    private long createPage(Space space, String title, String content, String label)
    {
        Page page = new Page(space, title, content);

        long pageId = rpc.createPage(page);

        if (isNotBlank(label))
        {
            rpc.addLabel(label, page);
        }

        return pageId;
    }

    private void viewPageById(long pageId)
    {
        gotoPage("/pages/viewpage.action?pageId=" + pageId);
    }

    private void assertDetailsRow(int rowIndex, String ... expectedCellValues)
    {
        for (int i = 0, j = expectedCellValues.length; i < j; ++i)
        {
            assertEquals(
                    expectedCellValues[i],
                    getElementTextByXPath("//div[@class='plugin-tabmeta-details']//tbody/tr[" + (rowIndex + 1) + "]/td[" + (i + 1) + "]")
            );
        }
        assertElementNotPresentByXPath("//div[@class='plugin-tabmeta-details']//tbody/tr[" + (rowIndex + 1) + "]/td[" + (expectedCellValues.length + 1) + "]");
    }

    private void assertSummaryHeading(String ... expectedHeadings)
    {

        for (int headingIndex = 0; headingIndex < expectedHeadings.length; ++headingIndex)
        {
            assertEquals(
                    expectedHeadings[headingIndex],
                    getElementTextByXPath(TABLE_XPATH_ROOT + "//tr/th[" + (headingIndex + 1) + "]")
            );
        }
    }

    private void assertSummaryRow(int rowIndex, String expectedContentTitle, String ... expectedValues)
    {
        assertEquals(
                expectedContentTitle,
                getElementTextByXPath(TABLE_XPATH_ROOT + "//tr[" + (rowIndex) + "]/td/a")
        );

        for (int columnIndex = 0; columnIndex < expectedValues.length; ++columnIndex)
        {
            assertEquals(
                    isBlank(expectedValues[columnIndex]) ? "" : String.format("%s", expectedValues[columnIndex]),
                    getElementTextByXPath(TABLE_XPATH_ROOT + "//tr[" + (rowIndex) + "]/td[" + (columnIndex + 2) + "]")
            );
        }
    }

    private void assertSummary(int row, int col, String xpath, String text)
    {
        assertEquals(
            text,
            getElementTextByXPath(TABLE_XPATH_ROOT + "//tr[" + (row) + "]/td[" + (col) + "]" + xpath)
        );
    }

}
