package it.com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.extra.masterdetail.utils.MarkupHelper;
import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.rpc.ConfluenceRpc;

public class MasterDetailsTestHelper
{
    private final ConfluenceRpc xhtmlRpc;

    public MasterDetailsTestHelper(ConfluenceRpc xhtmlRpc)
    {
        this.xhtmlRpc = xhtmlRpc;
    }

    public Page createPages(int pages, String label)
    {
        return createPages(pages, label, null, null);
    }

    public Page createPages(int pages, String label, String pageSize, String sortBy)
    {
        String content = createMetadata();
        xhtmlRpc.logIn(User.ADMIN);

        for (int i=0; i<pages; i++)
        {
            Page page = new Page(Space.TEST, "Page " + i, content);
            xhtmlRpc.createPage(page);
            xhtmlRpc.addLabel(label, page);
        }

        Page summaryPage = new Page(Space.TEST, "Summary Page", MarkupHelper.makeSummaryMarkup(label, Space.TEST, pageSize, sortBy));
        xhtmlRpc.createPage(summaryPage);
        xhtmlRpc.flushIndexQueue();

        return summaryPage;
    }

    public static String createMetadata()
    {
        return MarkupHelper.makeDetailsMarkup("Text", "Some text",
            "Status", "<ac:structured-macro ac:name=\"status\"><ac:parameter " +
            "ac:name=\"colour\">Grey</ac:parameter><ac:parameter " +
            "ac:name=\"title\">GREY</ac:parameter></ac:structured-macro>");
    }
}
