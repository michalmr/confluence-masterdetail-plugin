package it.com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.extra.masterdetail.utils.MarkupHelper;
import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.rpc.ConfluenceRpc;
import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

public class OldLabelsParameterTestCase extends AbstractConfluencePluginWebTestCase
{
    public static final String LABEL = "mylabel";
    private static final Space TEST_SPACE = new Space("FOO", "Foo Space", "Test Space Description");

    private ConfluenceRpc rpc;

    @Before
    @Override
    public void setUp() throws Exception
    {
        super.setUp();

        rpc = ConfluenceRpc.newInstance(getConfluenceWebTester().getBaseUrl());
        rpc.logIn(User.ADMIN);
        rpc.removeSpace(TEST_SPACE.getKey());
        rpc.createSpace(TEST_SPACE);
    }

    @Test
    public void testOldLabelsParameterIgnored()
    {
        Map<String, String> properties = MarkupHelper.buildMetadataMap("foo", "bar");
        Map<String, String> properties2 = MarkupHelper.buildMetadataMap("hello", "muppets");
        Page page = new Page(TEST_SPACE, "Page with page properties old label parameter",
            pagePropertiesStorageFormat(properties, LABEL) +
            pagePropertiesStorageFormat(properties2, null)
        );

        rpc.createPage(page);
        rpc.addLabel(LABEL, page);

        String summary = MarkupHelper.makeSummaryMarkup(LABEL, TEST_SPACE.getKey());
        Page summaryPage = new Page(TEST_SPACE, "Summary Page", summary);
        rpc.createPage(summaryPage);

        gotoPage(summaryPage.getUrl());
        assertTextPresent("foo");
        assertTextPresent("muppets");

        String summary2 = MarkupHelper.makeSummaryMarkup(LABEL, LABEL, TEST_SPACE.getKey(), null, null, null, null);
        Page summaryPage2 = new Page(TEST_SPACE, "Summary Page2", summary2);
        rpc.createPage(summaryPage2);

        gotoPage(summaryPage2.getUrl());
        assertTextPresent("No content found");
    }

    private String pagePropertiesStorageFormat(final Map<String, String> properties, final String label)
    {
        StringBuilder macro = new StringBuilder("<ac:macro ac:name=\"details\">");
        if (label == null)
            macro.append("<ac:parameter ac:name=\"label\" />");
        else
            macro.append("<ac:parameter ac:name=\"label\">").append(label).append("</ac:parameter>");

        macro.append("<ac:rich-text-body>");
        if (properties != null && properties.size() > 0)
        {
            macro.append("<table><tbody>");
            for (Map.Entry<String, String> property : properties.entrySet())
            {
                macro.append("<tr>");
                macro.append("<th>").append(property.getKey()).append("</th>");
                macro.append("<td>").append(property.getValue()).append("</td>");
                macro.append("</tr>");
            }
            macro.append("</tbody></table>");
        }
        macro.append("</ac:rich-text-body>").append("</ac:macro>");

        return macro.toString();
    }
}
