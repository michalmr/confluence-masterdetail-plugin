package it.com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.extra.masterdetail.utils.MarkupHelper;
import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.rpc.ConfluenceRpc;
import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static com.atlassian.confluence.extra.masterdetail.utils.MarkupHelper.*;

/**
 * Tests that Page Properties for the *correct pages* are displayed in the report.
 *
 * This test class exists to pull filtering-specific tests out of the monolithic {@link MasterDetailPluginTestCase}.
 */
@RunWith(JUnit4.class)
public class PagePropertiesReportFilteringTestCase extends AbstractConfluencePluginWebTestCase
{
    private static final Space TEST_SPACE = new Space("tst", "Test Space", "Test Space Description");
    public static final String LABEL1 = "label1";

    private ConfluenceRpc rpc;

    @Before
    @Override
    public void setUp() throws Exception
    {
        super.setUp();

        rpc = ConfluenceRpc.newInstance(getConfluenceWebTester().getBaseUrl());
        rpc.logIn(User.ADMIN);
        rpc.removeSpace(TEST_SPACE.getKey());
        rpc.createSpace(TEST_SPACE);
    }

    // CONFDEV-17283
    @Test
    public void testTrashedPagesNotInReport()
    {
        Page keptPage = new Page(TEST_SPACE, "Page that is Kept", makeDefaultDetailsMarkup());
        Page trashedPage = new Page(TEST_SPACE, "Page that gets Trashed", makeDefaultDetailsMarkup());
        Page reportPage = new Page(TEST_SPACE, "Report page", MarkupHelper.makeSummaryMarkup(LABEL1, ""));
        // 'spaces' parameter set to '@all' showing trashed pages is the CONFDEV-17283 issue
        Page reportPageAllSpaces = new Page(TEST_SPACE, "Report page All Spaces", makeSummaryMarkup(LABEL1, ALL_SPACES));

        rpc.createPage(keptPage);
        rpc.createPage(trashedPage);
        rpc.createPage(reportPage);
        rpc.createPage(reportPageAllSpaces);
        rpc.addLabel(LABEL1, keptPage);
        rpc.addLabel(LABEL1, trashedPage);

        viewPage(reportPage);
        assertPageLinkPresent(keptPage);
        assertPageLinkPresent(trashedPage);

        viewPage(reportPageAllSpaces);
        assertPageLinkPresent(keptPage);
        assertPageLinkPresent(trashedPage);

        // Now remove a page - it should not appear in either report page.
        rpc.removePage(trashedPage);

        viewPage(reportPage);
        assertPageLinkPresent(keptPage);
        assertPageLinkNotPresent(trashedPage);

        viewPage(reportPageAllSpaces);
        assertPageLinkPresent(keptPage);
        assertPageLinkNotPresent(trashedPage);
    }

    private void viewPage(Page page)
    {
        gotoPage(page.getUrl());
    }

    private void assertPageLinkPresent(Page page)
    {
        assertLinkPresentWithText(page.getTitle());
    }

    private void assertPageLinkNotPresent(Page page)
    {
        assertLinkNotPresentWithText(page.getTitle());
    }
}
