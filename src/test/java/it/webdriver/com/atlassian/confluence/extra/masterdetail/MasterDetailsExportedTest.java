package it.webdriver.com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.extra.masterdetail.utils.MarkupHelper;
import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.rpc.ConfluenceRpc;
import com.atlassian.confluence.util.test.annotations.ExportedTestClass;
import com.atlassian.confluence.webdriver.AbstractWebDriverTest;
import com.google.common.collect.ImmutableMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

@ExportedTestClass
public class MasterDetailsExportedTest extends AbstractWebDriverTest
{
    private ConfluenceRpc xhtmlRpc;

    @Before
    public void setup()
    {
        xhtmlRpc = ConfluenceRpc.newInstance(rpc.getBaseUrl(), ConfluenceRpc.Version.V2);
        xhtmlRpc.logIn(User.ADMIN);
    }

    @After
    public void tearDown()
    {
        xhtmlRpc.logOut();
    }

    @Test
    public void testPagePropertiesReport()
    {
        final String label = "mylabel";

        Page summaryPage = new Page(Space.TEST, "Properties report", MarkupHelper.makeSummaryMarkup(label, Space.TEST));
        xhtmlRpc.createPage(summaryPage);

        Page propertiesPage = new Page("Page with properties",
                MarkupHelper.makeDetailsMarkup(null, false, "Foo", "Bar"), summaryPage);
        xhtmlRpc.createPage(propertiesPage);
        xhtmlRpc.addLabel(label, propertiesPage);

        xhtmlRpc.flushIndexQueue();

        product.loginAndView(User.TEST, summaryPage);
        MasterDetailsSummary summary = product.getPageBinder().bind(MasterDetailsSummary.class);
        assertEquals(summary.getNumberOfRows(), 1);
        assertTrue(summary.getTitles().contains(propertiesPage.getTitle()));
    }

}
