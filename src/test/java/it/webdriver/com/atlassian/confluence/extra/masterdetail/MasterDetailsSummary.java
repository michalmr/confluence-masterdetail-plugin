package it.webdriver.com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.pageobjects.component.ConfluenceAbstractPageComponent;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.webdriver.utils.by.ByJquery;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;

import javax.annotation.Nullable;
import java.util.List;

public class MasterDetailsSummary extends ConfluenceAbstractPageComponent
{
    @ElementBy(className = "metadata-summary-macro")
    private PageElement summaryTable;

    @ElementBy(cssSelector = "ol.aui-nav-pagination")
    private PageElement paginationContainer;

    @ElementBy(cssSelector = "ol.aui-nav-pagination .aui-nav-previous > a")
    private PageElement previous;

    @ElementBy(cssSelector = "ol.aui-nav-pagination .aui-nav-next > a")
    private PageElement next;

    public TimedCondition isPaginationVisible()
    {
        return paginationContainer.timed().isVisible();
    }

    /**
     * Returns the default title column as a list of Strings
     */
    public List<String> getTitles()
    {
        return Lists.transform(summaryTable.findAll(By.cssSelector("tbody td.title")),
            new Function<PageElement, String>()
            {
                @Override
                public String apply(@Nullable PageElement input)
                {
                    return input.getText();
                }
            });
    }

    public MasterDetailsSummary clickSort(String columnHeading)
    {
        PageElement header = summaryTable.find(ByJquery.$("th.sortableHeader:contains(" + columnHeading + ")"));
        header.click();
        return this;
    }

    public int getNumberOfRows()
    {
        return summaryTable.findAll(By.cssSelector("tbody tr")).size();
    }


    public int getPaginationSize()
    {
        return paginationContainer.findAll(ByJquery.$("li[data-index]")).size();
    }

    public MasterDetailsSummary clickPrevious()
    {
        previous.click();
        return this;
    }

    public MasterDetailsSummary clickNext()
    {
        next.click();
        return this;
    }

    public TimedCondition isIndexSelected(int index)
    {
        return getIndex(index).timed().hasClass(
            "aui-nav-selected");
    }

    public MasterDetailsSummary click(int index)
    {
        getIndex(index).find(By.tagName("a")).click();
        return this;
    }

    public List<String> getColumnValues(String columnName)
    {
        int index = getIndexOfColumn(columnName);
        if (index == -1)
            throw new IllegalArgumentException("Column " + columnName + " does not exist");

        index++; // CSS nth-child is 1-based
        List<PageElement> values = summaryTable.findAll(By.cssSelector("tbody tr td:nth-child(" + index + ")"));
        return Lists.transform(values, new Function<PageElement, String>()
        {
            @Override
            public String apply(PageElement cell)
            {
                return StringUtils.trim(cell.getText());
            }
        });
    }

    private int getIndexOfColumn(String columnName)
    {
        List<PageElement> columns = summaryTable.findAll(By.cssSelector("thead tr:first-child th"));
        for (int i = 0; i < columns.size(); ++i)
        {
            PageElement column = columns.get(i);
            if (column.getText().equals(columnName))
                return i;
        }

        return -1;
    }

    private PageElement getIndex(int index)
    {
        return paginationContainer.find(ByJquery.$("li[data-index=" + (index - 1) + "]"));
    }
}
