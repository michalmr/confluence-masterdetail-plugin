package it.webdriver.com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.extra.masterdetail.utils.MarkupHelper;
import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.rpc.ConfluenceRpc;
import com.atlassian.confluence.pageobjects.page.content.ViewPage;
import com.atlassian.confluence.webdriver.AbstractWebDriverTest;
import com.atlassian.pageobjects.elements.query.Poller;
import com.google.common.collect.ImmutableList;
import it.com.atlassian.confluence.extra.masterdetail.MasterDetailsTestHelper;
import org.junit.Before;
import org.junit.Test;

import static it.com.atlassian.confluence.extra.masterdetail.MasterDetailsTestHelper.createMetadata;
import static junit.framework.Assert.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class MasterDetailsSummaryTest extends AbstractWebDriverTest
{
    private ConfluenceRpc xhtmlRpc;
    private MasterDetailsTestHelper helper;

    @Before
    public void setup()
    {
        xhtmlRpc = ConfluenceRpc.newInstance(rpc.getBaseUrl(), ConfluenceRpc.Version.V2);
        helper = new MasterDetailsTestHelper(xhtmlRpc);
    }

    @Test
    public void testPagination()
    {
        String label = "foo";

        Page summaryPage = helper.createPages(100, label);

        product.loginAndView(User.TEST, summaryPage);
        MasterDetailsSummary summary = product.getPageBinder().bind(MasterDetailsSummary.class);
        Poller.waitUntilTrue(summary.isPaginationVisible());

        // make sure pagination is setup correctly
        Poller.waitUntilTrue(summary.isIndexSelected(1));
        assertEquals(4, summary.getPaginationSize());
        assertTrue(summary.getTitles().contains("Page 99"));

        // go next twice
        summary.clickNext();
        Poller.waitUntilTrue(summary.isIndexSelected(2));
        assertTrue(summary.getTitles().contains("Page 69"));
        summary.clickNext();
        Poller.waitUntilTrue(summary.isIndexSelected(3));
        assertTrue(summary.getTitles().contains("Page 39"));

        // go previous once
        summary.clickPrevious();
        Poller.waitUntilTrue(summary.isIndexSelected(2));
        assertTrue(summary.getTitles().contains("Page 69"));

        // click on an index page
        summary.click(4);
        Poller.waitUntilTrue(summary.isIndexSelected(4));
        assertTrue(summary.getTitles().contains("Page 1"));
    }

    @Test
    public void testSortingTitle()
    {
        String label = "foo";

        Page summaryPage = helper.createPages(20, label, "5", "title");

        product.loginAndView(User.TEST, summaryPage);
        MasterDetailsSummary summary = product.getPageBinder().bind(MasterDetailsSummary.class);
        Poller.waitUntilTrue(summary.isPaginationVisible());

        Poller.waitUntilTrue(summary.isIndexSelected(1));
        assertEquals(4, summary.getPaginationSize());
        assertTrue(summary.getTitles().contains("Page 1"));
        assertTrue(summary.getTitles().contains("Page 11"));
        assertFalse(summary.getTitles().contains("Page 6"));

        summary.clickNext();
        Poller.waitUntilTrue(summary.isIndexSelected(2));
        assertTrue(summary.getTitles().contains("Page 13"));
        assertTrue(summary.getTitles().contains("Page 16"));

        summary.click(4);
        Poller.waitUntilTrue(summary.isIndexSelected(4));
        assertTrue(summary.getTitles().contains("Page 9"));
    }

    @Test
    public void testSortingAndPagination()
    {
        String label = "foo";

        Page summaryPage = helper.createPages(10, label, "2", "title");

        product.loginAndView(User.TEST, summaryPage);
        MasterDetailsSummary summary = product.getPageBinder().bind(MasterDetailsSummary.class);
        Poller.waitUntilTrue(summary.isPaginationVisible());
        Poller.waitUntilTrue(summary.isIndexSelected(1));
        assertEquals(2, summary.getNumberOfRows());
        assertEquals(5, summary.getPaginationSize());

        summary.clickSort("Title");
        assertEquals(2, summary.getNumberOfRows());
        assertTrue(summary.getTitles().contains("Page 0"));
        assertTrue(summary.getTitles().contains("Page 1"));

        summary.clickNext();
        Poller.waitUntilTrue(summary.isIndexSelected(2));
        summary.clickSort("Title");
        assertEquals(2, summary.getNumberOfRows());
        assertTrue(summary.getTitles().contains("Page 2"));
        assertTrue(summary.getTitles().contains("Page 3"));
    }

    @Test
    public void testDetailsUpdateNotCached()
    {
        String label = "metadata";
        Page metadataPage = new Page(Space.TEST, "Page with metadata", createMetadata());
        Page metadataPage2 = new Page(Space.TEST, "Page with metadata2", createMetadata());
        Page summaryPage = new Page(Space.TEST, "Page with metadata summary", MarkupHelper.makeSummaryMarkup(label,
            Space.TEST));

        xhtmlRpc.logIn(User.ADMIN);
        xhtmlRpc.createPage(metadataPage);
        xhtmlRpc.createPage(metadataPage2);
        xhtmlRpc.addLabel(label, metadataPage);
        xhtmlRpc.addLabel(label, metadataPage2);
        xhtmlRpc.createPage(summaryPage);
        xhtmlRpc.flushIndexQueue();

        ViewPage viewPage = product.loginAndView(User.TEST, summaryPage);
        MasterDetailsSummary summary = product.getPageBinder().bind(MasterDetailsSummary.class);
        Poller.waitUntilFalse(summary.isPaginationVisible());

        String text = viewPage.getTextContent();
        assertTrue(text.contains(metadataPage.getTitle()));
        assertTrue(text.contains(metadataPage2.getTitle()));
        assertTrue(text.contains("Some text"));

        metadataPage.setContent("<ac:structured-macro ac:name=\"details\"><ac:rich-text-body>\n" +
            "<table><tbody>\n" +
            "<tr>\n" +
            "<th colspan=\"1\">Text</th>\n" +
            "<td colspan=\"1\">Updated text</td>" +
            "</tr>\n" +
            "</tbody></table>" +
            "</ac:rich-text-body></ac:structured-macro>");

        xhtmlRpc.savePage(metadataPage); // edit one page
        xhtmlRpc.removePage(metadataPage2); // remove one page
        xhtmlRpc.flushIndexQueue();

        viewPage = product.viewPage(summaryPage.getIdAsString());
        text = viewPage.getTextContent();
        assertTrue(text.contains(metadataPage.getTitle()));
        assertFalse(text.contains(metadataPage2.getTitle()));
        assertFalse(text.contains("Some text"));
        assertTrue(text.contains("Updated text"));
    }

    @Test
    public void testMultiplePropertiesAndReportById()
    {
        xhtmlRpc.logIn(User.ADMIN);
        Page summaryPage = new Page(Space.TEST, "Properties report",
            MarkupHelper.makeSummaryMarkup("mylabel1", "myId1", Space.TEST.getKey(), "", "Text,Status,Foo1,Foo2,Foo3",
                null, null)
        );
        xhtmlRpc.createPage(summaryPage);

        Page propertiesPage = new Page("Page with multiple properties",
                MarkupHelper.makeDetailsMarkup("myId1",false, "Foo1", "Bar1") +
                MarkupHelper.makeDetailsMarkup(null, false, "Foo2", "Bar2") +
                MarkupHelper.makeDetailsMarkup(null, false, "Foo3", "Bar3"), summaryPage);
        xhtmlRpc.createPage(propertiesPage);
        xhtmlRpc.addLabel("mylabel1", propertiesPage);
        xhtmlRpc.addLabel("mylabel2", propertiesPage);
        xhtmlRpc.addLabel("mylabel3", propertiesPage);

        xhtmlRpc.flushIndexQueue();

        product.loginAndView(User.TEST, summaryPage);
        MasterDetailsSummary summary = product.getPageBinder().bind(MasterDetailsSummary.class);
        assertEquals(summary.getNumberOfRows(), 1);
        assertTrue(summary.getTitles().contains(propertiesPage.getTitle()));
        assertEquals(ImmutableList.of("Bar1"), summary.getColumnValues("Foo1"));
        assertEquals(ImmutableList.of(""), summary.getColumnValues("Foo2"));
        assertEquals(ImmutableList.of(""), summary.getColumnValues("Foo3"));

        // no ID should get all properties on a page
        summaryPage.setContent(MarkupHelper.makeSummaryMarkup("mylabel2", "", Space.TEST.getKey(), "",
            "Text,Status,Foo1,Foo2,Foo3", null, null));
        xhtmlRpc.updatePage(summaryPage, false, "");

        product.viewPage(summaryPage.getIdAsString());
        summary = product.getPageBinder().bind(MasterDetailsSummary.class);
        assertEquals(1, summary.getNumberOfRows());
        assertTrue(summary.getTitles().contains(propertiesPage.getTitle()));
        assertEquals(ImmutableList.of("Bar1"), summary.getColumnValues("Foo1"));
        assertEquals(ImmutableList.of("Bar2"), summary.getColumnValues("Foo2"));
        assertEquals(ImmutableList.of("Bar3"), summary.getColumnValues("Foo3"));
    }
}
