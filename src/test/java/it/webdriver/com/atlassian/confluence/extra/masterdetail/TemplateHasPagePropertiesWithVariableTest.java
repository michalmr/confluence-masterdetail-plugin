package it.webdriver.com.atlassian.confluence.extra.masterdetail;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.Matchers.containsString;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.rpc.ConfluenceRpc;
import com.atlassian.confluence.pageobjects.page.admin.templates.EditGlobalTemplatePage;
import com.atlassian.confluence.pageobjects.page.admin.templates.GlobalTemplatesPage;
import com.atlassian.confluence.pageobjects.page.admin.templates.TemplateInfo;
import com.atlassian.confluence.pageobjects.page.admin.templates.ViewTemplatePage;
import com.atlassian.confluence.pageobjects.page.content.CreatePage;
import com.atlassian.confluence.pageobjects.page.content.PageTemplateWizard;
import com.atlassian.confluence.webdriver.AbstractWebDriverTest;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;

public class TemplateHasPagePropertiesWithVariableTest extends AbstractWebDriverTest
{
    private ConfluenceRpc xhtmlRpc;

    private long templateId;
    private final String TEMPLATE_NAME = "test_template1";
    private final String VARIABLE_NAME = "status";
    private final String VARIABLE_VALUE = "done";

    @Before
    public void setUp()
    {
        xhtmlRpc = ConfluenceRpc.newInstance(rpc.getBaseUrl(), ConfluenceRpc.Version.V2);

        String templateMarkup = createTemplateMarkupWithPagePropertiesAndVariable();

        xhtmlRpc.logIn(User.ADMIN);
        
        templateId = xhtmlRpc.createTemplate(TEMPLATE_NAME, templateMarkup, null);
    }

    @After
    public void tearDown()
    {
    }

    @Test
    public void testCreatePageFromTempalteHasPagePropertiesWithVariable()
    {
        PageTemplateWizard pageTemplateWizard = product.login(User.ADMIN, PageTemplateWizard.class, Space.TEST,
                templateId);
        pageTemplateWizard.enterTextVariable(VARIABLE_NAME, VARIABLE_VALUE);
        CreatePage newPage = pageTemplateWizard.next();

        TimedQuery<String> contentHtml = newPage.getContent().getTimedHtml();
        waitUntil(contentHtml, containsString(VARIABLE_VALUE));
    }

    @Test
    public void testViewTempalteHasPagePropertiesWithVariable()
    {

        GlobalTemplatesPage globalTempagesPage = product.login(User.ADMIN, GlobalTemplatesPage.class);
        TemplateInfo<EditGlobalTemplatePage> templateInfo = globalTempagesPage.getTemplates().get(TEMPLATE_NAME);
        ViewTemplatePage viewTemplatePage = templateInfo.viewTemplate();
        String contentElementText = viewTemplatePage.getContentElementText();
        assertTrue(contentElementText.contains(VARIABLE_NAME));
    }

    @Test
    public void testPreviewTempalteHasPagePropertiesWithVariable()
    {

        GlobalTemplatesPage globalTempagesPage = product.login(User.ADMIN, GlobalTemplatesPage.class);
        TemplateInfo<EditGlobalTemplatePage> templateInfo = globalTempagesPage.getTemplates().get(TEMPLATE_NAME);
        EditGlobalTemplatePage editGlobalTemplatePage = templateInfo.editTemplate();
        editGlobalTemplatePage.preview();
        TimedCondition timedCondition = editGlobalTemplatePage.hasPreviewContent(VARIABLE_NAME);
        Poller.waitUntilTrue(timedCondition);
    }

    private String createTemplateMarkupWithPagePropertiesAndVariable()
    {
        // The file must have variable that is the same with VARIABLE_NAME
        return getDumpXmlData("template-with-editor-format.xml");
    }
    
    private String getDumpXmlData(String dumpFileName)
    {
        String xmlData = null;
        if (xmlData == null)
        {
            InputStream xmlStream = this.getClass().getClassLoader().getResourceAsStream("xml/" + dumpFileName);
            try
            {
                xmlData = IOUtils.toString(xmlStream, "UTF-8");
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
        }

        return xmlData;
    }
}